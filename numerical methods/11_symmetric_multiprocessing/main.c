#include"header.h"
#define RANDOM_NUMBER ((double) rand()/RAND_MAX) // random number from 0 to 1

int main(){
	printf("\nMulti-threaded version of \nlinear equations exercise task B using OpenMP:\n");
	printf("Matrix inverse by Gram-Schmidt QR factorization \n");

// allocate memory to matrices and vectors 
	size_t n = 10;
	gsl_matrix *A = gsl_matrix_calloc(n,n);
	gsl_matrix *Q = gsl_matrix_calloc(n,n);
	gsl_matrix *Q_omp = gsl_matrix_calloc(n,n);
	gsl_matrix *R = gsl_matrix_calloc(n,n);
	gsl_matrix *R_omp = gsl_matrix_calloc(n,n);
	gsl_matrix *A_inv = gsl_matrix_calloc(n,n);
	gsl_matrix *A_inv_omp = gsl_matrix_calloc(n,n);
	gsl_matrix* AA_inv = gsl_matrix_calloc(n,n);

// generate matrix A and vector b 
	for(int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(A,i,j,RANDOM_NUMBER);
		}
	}
	/*
	printf("\nrandom square matrix: \nA = ");
	print_matrix(A);
	*/

// QR decomposition
	printf("\nQR decomposition\n");
	gsl_matrix_memcpy(Q,A); // makes a copy of A and call it Q, because qr_decomposition alters A to make Q
	clock_t begin_time = clock();
	qr_decomposition_omp(Q,R);
	clock_t end_time = clock();
	double time_spend = (double)(end_time - begin_time)/CLOCKS_PER_SEC;
	printf("\ttime without omp: %g seconds \n",time_spend);
	/*
	printf("\nQR decomposition: \nQ = ");
	print_matrix(Q);
	printf("\nR = ");
	print_matrix(R);
	*/

// omp: QR decomposition 
	gsl_matrix_memcpy(Q_omp,A); // makes a copy of A and call it Q, because qr_decomposition alters A to make Q
	begin_time = clock();
	qr_decomposition_omp(Q_omp,R_omp);
	end_time = clock();
	time_spend = (double)(end_time - begin_time)/CLOCKS_PER_SEC;
	printf("\ttime with omp: %g seconds \n",time_spend);
	/*
	printf("\nQR decomposition: \nomp: Q = ");
	print_matrix(Q_omp);
	printf("\nomp: R = ");
	print_matrix(R_omp);
	*/
	
// inverse of A 
	printf("\nQR inverse\n");
	begin_time = clock();
	qr_inverse(Q, R, A_inv);
	end_time = clock();
	time_spend = (double)(end_time - begin_time)/CLOCKS_PER_SEC;
	printf("\ttime without omp: %g seconds \n",time_spend);
	/*
	printf("\nmatrix inverse: \nA_inv = ");
	print_matrix(A_inv);
	printf("\n");
	*/

// omp: inverse of A 
	begin_time = clock();
	qr_inverse(Q_omp, R_omp, A_inv_omp);
	end_time = clock();
	time_spend = (double)(end_time - begin_time)/CLOCKS_PER_SEC;
	printf("\ttime with omp: %g seconds \n",time_spend);
	/*
	printf("\nmatrix inverse: \nA_inv = ");
	print_matrix(A_inv_omp);
	printf("\n");
	*/
	

// test result 
	/*
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, A_inv, 0.0, AA_inv); 

	printf("test: is A A_inv = I, the identity matrix?\nA A_inv = ");
	print_matrix(AA_inv);
	printf("test result: yes");
	printf("\n\n");
	*/

// free memory 
	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_matrix_free(Q_omp);
	gsl_matrix_free(R);
	gsl_matrix_free(R_omp);
	gsl_matrix_free(A_inv);
	gsl_matrix_free(A_inv_omp);
	gsl_matrix_free(AA_inv);

	return 0;
}