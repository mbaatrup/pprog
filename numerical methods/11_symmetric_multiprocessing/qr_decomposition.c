#include"header.h"

void qr_decomposition_omp(gsl_matrix* A, gsl_matrix* R){
/* performs in-place modified Gram-Schmidt orthogonalization of an n×m (n≥m) matrix A: 
A turns into Q and the square m×m matrix R is computed */
	int m = A->size2;
#pragma omp parallel sections
{
	#pragma omp section
	{
	for(int j = 0; j < m/2; j++){
		// diagonal elements of R
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		// off-diagonal elements of R
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
	}
	#pragma omp section
	{
	for(int j = m/2; j < m; j++){
		// diagonal elements of R
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		// off-diagonal elements of R
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
	}
}
}

void qr_decomposition(gsl_matrix* A, gsl_matrix* R){
/* performs in-place modified Gram-Schmidt orthogonalization of an n×m (n≥m) matrix A: 
A turns into Q and the square m×m matrix R is computed */
	int m = A->size2;
	for(int j = 0; j < m; j++){
		// diagonal elements of R
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		// off-diagonal elements of R
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
}