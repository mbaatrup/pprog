#ifndef HEADER
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#include<omp.h>
#include<time.h>

/* generate random matrix (fill with random numbers) */
void random_matrix(gsl_matrix* M);

/* print vectors and matrices */
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);

/* QR decomposition */
void qr_decomposition(gsl_matrix* A, gsl_matrix* R);
void qr_decomposition_omp(gsl_matrix* A, gsl_matrix* R);

/* solve linear equal QRx = b */
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

/* calculates the inverse of matrix A = QR and saves the result in A_inv*/
void qr_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv);
void qr_inverse_omp(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv);

#define HEADER
#endif