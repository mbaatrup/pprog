#include"header.h"
#define PRINT_FORMAT "%7.3f"

void print_vector(gsl_vector *v){
	printf("\n");
	for(int element = 0; element < v->size; element++){
			printf(PRINT_FORMAT, gsl_vector_get(v,element));
			printf("\n");
		}
}

void print_matrix(gsl_matrix *M){
	printf("\n");
	for(int row = 0; row < M->size1; row++){
		for (int column = 0; column < M->size2; column++){
			printf(PRINT_FORMAT, gsl_matrix_get(M,row,column));
		}
		printf("\n");
	}
}