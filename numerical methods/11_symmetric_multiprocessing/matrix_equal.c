#include"header.h"

int matrix_equal(gsl_matrix* A, gsl_matrix* B, double epsabs, double epsrel){

	assert(A->size1 == B->size1 && A->size2 == B->size2);

	int result;
	for(int i = 0; i < A->size1; i++){
		for(int j = 0; j < A ->size2; j++){
			double a = fabs(gsl_matrix_get(A,i,j) - gsl_matrix_get(B,i,j));
			double b = fabs(gsl_matrix_get(A,i,j)) + fabs(gsl_matrix_get(B,i,j));
			if(a < epsabs){result = 1;}
			else if(a/b < epsrel/2){result = 1;}
			else{result = 0;}
		}
	}
	return result;
}