#include"header.h" 

void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
/* solves the equation QRx=b by applying Q^T to the vector b (saving the result in x) 
and then performing in-place back-substitution on x */

	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x); // applying Q^T to b and saving the result in x

	// in-place back-substitution:
	int n = x->size;
	for(int j = n - 1; j >= 0; j--){
		double sum = 0;
		for(int k = j + 1; k < n; k++){
			sum += gsl_matrix_get(R,j,k)*gsl_vector_get(x,k);
		}
		double x_j = (gsl_vector_get(x,j) - sum) / gsl_matrix_get(R,j,j);
		gsl_vector_set(x,j,x_j);
	}

}