
Multi-threaded version of 
linear equations exercise task B using OpenMP:
Matrix inverse by Gram-Schmidt QR factorization 

QR decomposition
	time without omp: 0.010052 seconds 
	time with omp: 0.007207 seconds 

QR inverse
	time without omp: 3.4e-05 seconds 
	time with omp: 9e-06 seconds 
