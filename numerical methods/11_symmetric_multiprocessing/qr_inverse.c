#include"header.h"

void qr_inverse_omp(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv){
	/* calculates the inverse of the matrix A and saves the result in A_inv */
	int n = Q->size1;
	gsl_vector *x  = gsl_vector_calloc(n);
	gsl_vector *b  = gsl_vector_calloc(n);

	#pragma omp parallel sections
	{
	#pragma omp section
	{
		for(int i = 0; i < n/2; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_solve(Q,R,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
		}
	}

	#pragma omp section
	{
		for(int i = n/2; i < n; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_solve(Q,R,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
		}
	}
	}
	
	gsl_vector_free(x);
	gsl_vector_free(b);
}

void qr_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv){
	/* calculates the inverse of the matrix A and saves the result in A_inv */
	int n = Q->size1;
	gsl_vector *x  = gsl_vector_calloc(n);
	gsl_vector *b  = gsl_vector_calloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_solve(Q,R,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
	}
	gsl_vector_free(x);
	gsl_vector_free(b);
}