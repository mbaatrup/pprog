#include"header.h"
// embedded midpoint-Euler (Runge-Kutta 12) stepper
void rkstep12(
	double t,                                  		// the current value of the variable 
	double h,                                 	 	// the step to be taken 
	gsl_vector* y,                                	// the current value y(t) of the sought function 
	void f(double t, gsl_vector* y, gsl_vector* dydt), // the right-hand-side, dydt = f(t,y) 
	gsl_vector* yy,                                	// output: y(t + h) 
	gsl_vector* err)                               	// output: error estimate dy 
	{
	int n = y->size;
	// allocate memory
	gsl_vector* k_0 = gsl_vector_alloc(n);
	gsl_vector* k_half = gsl_vector_alloc(n);
	// fill yy
	gsl_vector_memcpy(yy,y);
	gsl_blas_daxpy(h/2, k_0, y); // y = h/2 *k_0 + y
	f(t + h/2, y, k_half); // fill k_half
	gsl_blas_daxpy(h, k_half, yy); // yy = h *k_half + y
	// fill err
	gsl_vector_memcpy(err, k_0);
	gsl_vector_sub(err, k_half); // err = k_0 - k_half
	gsl_vector_scale(err, h); // err = h* err
	// free memory
	gsl_vector_free(k_0);
	gsl_vector_free(k_half);
}
