#include"header.h"
#define FMT "%.8e"

int driver(
	gsl_matrix* P,                          // path matrix - the current value of t, y, dydt
	double b,                              	// the end-point of the integration
	double *h,                             	// the current step-size 
	double acc,                            	// absolute accuracy goal 
	double eps,                            	// relative accuracy goal 
	void stepper(							// the stepper function to be used 
		double t, double h, gsl_vector* y,
		void f(double t, gsl_vector* y, gsl_vector* dydt), 
		gsl_vector* yy, gsl_vector* err), 	
	void f(double t, gsl_vector* y, gsl_vector* dydt)) // right-hand-side of differential equation
	{
	// allocate memory
	int n = P->size2 - 1; 
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* yy = gsl_vector_alloc(n); // filled by stepper
	gsl_vector* err = gsl_vector_alloc(n); // filled by stepper
	// taking steps
	int steps = 0, steps_max = P->size1;
	while(gsl_matrix_get(P, steps, 0) < b){ 
		double t = gsl_matrix_get(P, steps, 0);
		for(int i = 0; i < n; i++){
			gsl_vector_set(y, i, gsl_matrix_get(P, steps, i + 1)); 
		}
		if(fabs(t + *h) > fabs(b)){
			*h = b - t; // adjust size of last step to hit the end-point
		}
		stepper(t, *h, y, f, yy, err); // take a step
		// test step
		double err_local = gsl_blas_dnrm2(err); // local error
		double tol_local = acc + eps *gsl_blas_dnrm2(yy); // local tolerance
		if(err_local < tol_local){ // accept step and continue 
			steps++;
			if(steps > steps_max){
				printf("driver_path: maximum number of steps reached");
				break;
			}
			// store path
			gsl_matrix_set(P, steps, 0, t + *h); // new t
			for(int i = 0; i < n; i++){
				gsl_matrix_set(P, steps, i + 1, gsl_vector_get(yy,i)); // new y
			}
		} // otherwise adjust step
		if(err_local > 0){ // avoid dividing with zero
			*h *= pow(tol_local/err_local, 0.25) *0.95; // empirical prescription for step-size adjustment
		}
		else{
			*h *= 2; // if zero error, take larger step!;
		}
	}
	// free memory
	gsl_vector_free(y);
	gsl_vector_free(yy);
	gsl_vector_free(err);
	
	return steps;
}