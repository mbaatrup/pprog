#include"header.h"
#define FMT2e "%.2e"
#define FMT8e "%.8e"

void ode_logistic(double x, gsl_vector* f, gsl_vector* dfdx){
	double fx = gsl_vector_get(f,0);
	gsl_vector_set(dfdx,0, fx*(1.0 - fx));
}

void ode_sine(double t, gsl_vector* y, gsl_vector* dydt){ 
	gsl_vector_set(dydt,0, gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,- gsl_vector_get(y,0));
}

int main(){
	double acc = 1e-3, eps = 1e-3; // absolute and relative accuracy goals
	printf("Stepper: Embedded Runge-Kutta 21 (mid-point Euler method)\n");
	printf("Driver: Adaptive-step-size, storing the path\n\n");

	// allocate memory
	int n = 1;
	gsl_matrix* p = gsl_matrix_calloc(STEPS_MAX, n + 1); // path: x, f
	// start and end points
	double a = 0.0, b = 3.0; // lower and upper limit of integration
	gsl_matrix_set(p,0,0, a);
	gsl_matrix_set(p,0,1, 0.5);
	// initial step size and direction
	double Dx = (b - a)/1e2;
	// integration while storing path
	printf("ODE: f'(x) = f(x)*(1 - f(x))\n");
	printf("Integration inteval: [a,b] = ["FMT2e","FMT2e"]\n", a, b);
	printf("Initial condition: f(a) = "FMT2e"\n", 0.5);
	printf("Estimate of initial stepsize: Dx = "FMT2e"\n", Dx);
	int steps = driver(p, b, &Dx, acc, eps, rkstep12, ode_logistic);
	printf("Estimated solution: f(b) = "FMT2e"\n", gsl_matrix_get(p,steps,1));
	printf("Exact solution: [1 + exp(b)]⁻¹ = "FMT2e"\n", 1.0/(1.0 + exp(-b)));
	printf("Last accepted stepsize: Dx = "FMT2e"\n", Dx);
	printf("Accuracy goals: acc = %.0e, eps = %.0e\n", acc, eps);
	printf("Number of steps: %i\n\n", steps);
	// see data.txt and plot.svg
	fprintf(stderr,"x\t f\t f_exact\n");
	for(int i = 0; i < steps; i++){
		double x = gsl_matrix_get(p,i,0);
		double f = gsl_matrix_get(p,i,1);
		double f_exact = 1.0/(1.0 + exp(-x));
		fprintf(stderr,""FMT8e" \t"FMT8e" \t"FMT8e"\n", x, f, f_exact);
	}
	fprintf(stderr,"\n\n");
	// free memory
	gsl_matrix_free(p);
	

	// allocate memory
	n = 2;
	gsl_matrix* P = gsl_matrix_calloc(STEPS_MAX, n + 1); // path: t, y, dydt
	// start and end points
	a = 0.0, b = 2.0*M_PI; // lower and upper limit of integration
	gsl_matrix_set(P,0,0, a);
	gsl_matrix_set(P,0,1, sin(a));
	gsl_matrix_set(P,0,2, cos(a));
	// initial step size and direction
	double Dt = (b - a)/1e3;
	// integration while storing path
	printf("ODE: y''(t) = - y(t)\n");
	printf("Integration inteval: [a,b] = ["FMT2e","FMT2e"]\n", a, b);
	printf("Initial condition: y(a) = "FMT2e"\n", sin(a));
	printf("Estimate of initial stepsize: Dt = "FMT2e"\n", Dt);
	steps = driver(P, b, &Dt, acc, eps, rkstep12, ode_sine);
	printf("Estimated solution: y(b) = "FMT2e", y'(b) = "FMT2e"\n", gsl_matrix_get(P,steps,1), gsl_matrix_get(P,steps,2));
	printf("Exact solution: sin(b) = "FMT2e", cos(b) = "FMT2e"\n", sin(b), cos(b));
	printf("Last accepted stepsize: Dt = "FMT2e"\n", Dt);
	printf("Accuracy goals: acc = %.0e, eps = %.0e\n", acc, eps);
	printf("Number of steps: %i\n", steps);
	// see data.txt and plot.svg
	fprintf(stderr,"t\t y\t y_exact\n");
	for(int i = 0; i < steps; i++){
		double t = gsl_matrix_get(P,i,0);
		double y = gsl_matrix_get(P,i,1);
		double y_exact = sin(gsl_matrix_get(P,i,0));
		fprintf(stderr,""FMT8e" \t"FMT8e" \t"FMT8e"\n", t, y, y_exact);
	}
	fprintf(stderr,"\n\n");
	fprintf(stderr,"t\t y'\t y'_exact\n");
	for(int i = 0; i < steps; i++){
		double t = gsl_matrix_get(P,i,0);
		double dydt = gsl_matrix_get(P,i,2);
		double dydt_exact = cos(gsl_matrix_get(P,i,0));
		fprintf(stderr,""FMT8e" \t"FMT8e" \t"FMT8e"\n", t, dydt, dydt_exact);
	}
	fprintf(stderr,"\n\n");
	// free memory
	gsl_matrix_free(P);

	return 0;
}
