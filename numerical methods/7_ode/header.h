#ifndef HEADER
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#define STEPS_MAX 1e6

// vectors and matrices
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);

// ordinary differential equation system to be solved
void ode_system(double t, gsl_vector* y, gsl_vector* dydt);

// embedded midpoint-Euler (Runge-Kutta 12) stepper
void rkstep12(double t, double h, gsl_vector* y, 
	void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yy, gsl_vector* err);

// adaptive-step-size driver
int driver(gsl_matrix* P, double b, double *h, double acc, double eps, 
	void stepper(double t, double h, gsl_vector* y, 
	void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yy, gsl_vector* err),
	void f(double t, gsl_vector* y, gsl_vector* dydt));

#define HEADER
#endif
