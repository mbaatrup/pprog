#include"spline.h"
#define FMT "%8g"

int main(){

// data points
	int i, n = 8;
	double x[n], y[n];

	for(i = 0; i < n; i++){
		x[i] = i* 2.0*M_PI/(n - 1);
		y[i] = cos(x[i]);
		printf(""FMT" \t"FMT" \n", x[i], y[i]); // gnuplot index 0
	}
	printf("\n\n");

// exact values and linear interpolation 
	int nn = 100*n;
	double xx, dx = 2.0*M_PI/(nn-1);
 
	for(xx = x[0]; xx <= x[n-1]; xx += dx){
		double yy = linear_interpolation(n, x, y, xx);
		double yy_int = linear_interpolation_integral(n, x, y, xx);
		double yy_exact = cos(xx);
		double yy_int_exact = sin(xx);
		printf(""FMT" \t"FMT" \t"FMT" \t"FMT" \t"FMT" \n", xx, yy, yy_int, yy_exact, yy_int_exact); // gnuplot index 1
	}
	printf("\n\n");

// quadratic spline
	quadratic_spline * qs = quadratic_spline_alloc(n, x, y); // allocating and generating quadratic spline

	for(xx = x[0]; xx <= x[n - 1]; xx += dx){
		double yy = quadratic_spline_evaluate(qs, xx);
		double yy_int = quadratic_spline_integral(qs, xx);
		double yy_der = quadratic_spline_derivative(qs, xx);
		double yy_der_exact = -sin(xx);
		printf(""FMT" \t"FMT" \t"FMT" \t"FMT" \t"FMT" \n", xx, yy, yy_int, yy_der, yy_der_exact); // gnuplot index 2
	}
	printf("\n\n");

// cubic spline and gsl cubic spline
	cubic_spline * cs = cubic_spline_alloc(n, x, y); // allocating and generating cubic spline
	gsl_interp_accel *workspace = gsl_interp_accel_alloc(); // workspace
	gsl_spline *gsl_cs = gsl_spline_alloc(gsl_interp_cspline, n);
	gsl_spline_init(gsl_cs, x, y, n);

	for(xx = x[0]; xx <= x[n - 1]; xx += dx){
		double yy = cubic_spline_evaluate(cs, xx);
		double yy_gsl = gsl_spline_eval(gsl_cs, xx, workspace);
		printf(""FMT" \t"FMT" \t"FMT" \n", xx, yy, yy_gsl); // gnuplot index 3
	}
	printf("\n\n");

// free memory
	quadratic_spline_free(qs);
	cubic_spline_free(cs);
	gsl_interp_accel_free(workspace);
	gsl_spline_free(gsl_cs);
	
	return 0;
}