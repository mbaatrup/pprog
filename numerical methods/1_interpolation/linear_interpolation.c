#include"spline.h"

double linear_interpolation(int n, double *x, double *y, double xx){
	// make sure that there are at least two data points and that 
	// the interpolation points lie between the first and last data points:
	assert(n > 1 && xx >= x[0] && xx <= x[n - 1]); 
	// find index of data points closest to the interpolation point and choose the lower one 
	int i = binary_search(n, x, xx);
	// slope
	double p = (y[i + 1] - y[i])/(x[i + 1] - x[i]);
	// piecewise polynomial value as interpolation point
	double yy = y[i] + p*(xx - x[i]);

	return yy; // polynomial value at x = xx
}

double linear_interpolation_integral(int n, double *x, double *y, double xx){
	assert(n > 1 && xx >= x[0] && xx <= x[n - 1]);
	int i;
	double yy_int = 0, p = 0;

	for(i = 0; xx > x[i + 1]; i++){
		double h = x[i + 1] - x[i]; 
		assert(h > 0);
		p = (y[i + 1] - y[i])/h;
		yy_int += y[i]*h + 0.5*p*h*h;
	}
	double h=xx-x[i];
	p = (y[i + 1] - y[i])/h;
	yy_int += y[i]*h + 0.5*p*h*h;

	return yy_int; // integral of polynomial values from x = x_0 to x = xx
}
