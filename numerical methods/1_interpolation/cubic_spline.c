#include"spline.h"

cubic_spline * cubic_spline_alloc(int n, double *x, double *y){
// allocating memory
	cubic_spline *s = malloc(sizeof(cubic_spline));
	s->n = n;
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->b = malloc((n)*sizeof(double));
	s->c = malloc((n - 1)*sizeof(double));
	s->d = malloc((n - 1)*sizeof(double));
// data points
	for(int i = 0; i < n; i++){
		s->x[i] = x[i];
		s->y[i] = y[i];
	}
// inter-point distances and slopes
	double h[n - 1], p[n - 1];
	for(int i = 0; i < n - 1; i++){
		h[i] = x[i + 1] - x[i];
		p[i] = (y[i + 1] - y[i])/h[i];
	}
// building the tridiagonal system
	double D[n], Q[n - 1], B[n];
	D[0] = 2; D[n - 1] = 2; Q[0] = 1; B[0] = 3*p[0]; B[n - 1] = 3*p[n - 2];
	for(int i = 0; i < n - 2; i++){
		D[i + 1] = 2*h[i]/h[i + 1] + 2;
		Q[i + 1] = h[i]/h[i + 1];
		B[i + 1] = 3*(p[i] + p[i + 1]*h[i]/h[i + 1]);
	}
// Gauss elimination
	for(int i = 1; i < n; i++){
		D[i] -= Q[i - 1]/D[i - 1];
		B[i] -= B[i - 1]/D[i - 1];
	}
// back-substitution
	s->b[n - 1] = B[n - 1]/D[n - 1];
	for(int i = n - 2; i >= 0; i--){
		s->b[i] = (B[i] - Q[i]*s->b[i + 1])/D[i];
	}
// from continuity conditions
	for(int i = 0; i < n - 1; i++){
		s->c[i] = (-2*s->b[i] - s->b[i + 1] + 3*p[i])/h[i];
		s->d[i] = (s->b[i] + s->b[i + 1] - 2*p[i])/(h[i]*h[i]);
	}

	return s;
}

double cubic_spline_evaluate(cubic_spline *s, double xx){
	// make sure that there are at least two data points and that 
	// the interpolation points lie between the first and last data points:
	assert(s->n > 1 && xx >= s->x[0] && xx <= s->x[s->n - 1]); 
	// find index of data points closest to the interpolation point and choose the lower one:
	int i = binary_search(s->n, s->x, xx);

	// evaluate spline at xx between x[i] and x[i + 1]:
	double h = xx - s->x[i];
	double yy = s->y[i] + h*(s->b[i] + h*(s->c[i] + h*s->d[i]));

	return yy; // spline value at x = xx
}

void cubic_spline_free(cubic_spline *s){
	free(s);
}