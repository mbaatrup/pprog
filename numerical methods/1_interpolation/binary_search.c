#include"spline.h"

int binary_search(int n, double *x, double xx){
	// n is the number of data points, 
	// x is an array of data points, 
	// xx is an array of interpolation points.
	int i = 0, j = n - 1; // start boundaries
	while(j - i > 1){
		int m = (i + j)/2;
		if(xx > x[m]) i = m;
		else j = m;
	}
	return i;
}