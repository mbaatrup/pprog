#include"spline.h"

quadratic_spline * quadratic_spline_alloc(int n, double *x, double *y){
// allocating memory
	// m: number of data points
	// m-1: number of splines
	quadratic_spline *s = malloc(sizeof(quadratic_spline));
	s->n = n;
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->b = malloc((n - 1)*sizeof(double));
	s->c = malloc((n - 1)*sizeof(double));
	double* dx = malloc((n - 1)*sizeof(double));
	double* dy = malloc((n - 1)*sizeof(double));
	double* p = malloc((n - 1)*sizeof(double));
// data points, inter-point distances and slopes
	s->x = x;
	s->y = y;
	// m points: starting from m = 0 going to m = n-1
	for(int i = 0; i < n - 1; i++){
		dx[i] = x[i + 1] - x[i];
		dy[i] = y[i + 1] - y[i];
		p[i] = dy[i]/dx[i];
	}
// forward recursion from
	// m-1 splines: starting from m = 0... 
	s->b[0] = dy[0]/dx[0];
	s->c[0] = 0;
	// ... continuing from m = 1 to m = n-2
	for(int i = 1; i < n - 2; i++){
		s->b[i] = s->b[i - 1] - 2*s->c[i - 1]*dx[i - 1];
		s->c[i] = dy[i]/(dx[i]*dx[i]) - s->b[i]/dx[i];
	}
// backward recursion from m = n/2 (corresponding to finding the average of forward+backward recursion)
	int m = n-2;
	s->c[m] /= 2;
	s->b[m] = dy[m]/dx[m] - s->c[m] * dx[m];

	for (int i = m - 1; i >= 0; i--){
		s->b[i] = 2*dy[i]/dx[i] - s->b[i + 1];
		s->c[i] = dy[i]/(dx[i]*dx[i]) - s->b[i]/dx[i];
	}
// free memory
	free(dx);
	free(dy);
	free(p);
	return s;
}

double quadratic_spline_evaluate(quadratic_spline *s, double xx){
	// make sure that there are at least two data points and that 
	// the interpolation points lie between the first and last data points:
	assert(s->n > 1 && xx >= s->x[0] && xx <= s->x[s->n - 1]); 
	// find index of data points closest to the interpolation point and choose the lower one
	int i = binary_search(s->n, s->x, xx);

	// evaluate spline at xx between x[i] and x[i + 1]
	double yy = s->y[i] + s->b[i]*(xx - s->x[i]) + s->c[i]*(xx - s->x[i])*(xx - s->x[i]);

	return yy; // spline value at x = xx
}

double quadratic_spline_derivative(quadratic_spline *s, double xx){
	assert(s->n > 1 && xx >= s->x[0] && xx <= s->x[s->n - 1]);
	int i = binary_search(s->n, s->x, xx);
	double yy_der = s->b[i] + 2*s->c[i]*(xx - s->x[i]);
	return yy_der;
}

double quadratic_spline_integral(quadratic_spline *s, double xx){
	assert(s->n > 1 && xx >= s->x[0] && xx <= s->x[s->n - 1]);
	int i;
	double yy_int = 0, dx, dxx;

	for(i = 0; s->x[i + 1] < xx; i++){
		dx = s->x[i + 1] - s->x[i];
		yy_int += s->y[i]*dx + 1.0/2*s->b[i]*dx*dx + 1.0/3*s->c[i]*dx*dx*dx;
	}
	dxx = xx - s->x[i];
	yy_int += s->y[i]*dxx + 1.0/2*s->b[i]*dxx*dxx + 1.0/3*s->c[i]*dxx*dxx*dxx;

	return yy_int; // integral of polynomial values from x = x_0 to x = xx	
}

void quadratic_spline_free(quadratic_spline *s){
	free(s);
}