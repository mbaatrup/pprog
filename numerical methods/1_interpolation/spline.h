#ifndef SPLINE // if the header file is not already included, then include it
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
#include<gsl/gsl_spline.h>
#include<gsl/gsl_errno.h>

// binary search
int binary_search(int n, double *x, double xx);

// linear interpolation
double linear_interpolation(int n, double *x, double *y, double xx);
double linear_interpolation_integral(int n, double *x, double *y, double xx); // evaluates the integral of a linear interpolation from x[0] to xx

// quadratic spline
typedef struct {int n; double *x, *y, *b, *c;} quadratic_spline; // defines the quadratic spline structure
quadratic_spline * quadratic_spline_alloc(int n, double *x, double *y); // allocates and builds the quadratic spline
double quadratic_spline_evaluate(quadratic_spline *s, double xx); // evaluates the prebuilt spline at point xx
double quadratic_spline_derivative(quadratic_spline *s, double xx); // evaluates the derivative of the prebuilt spline at point xx
double quadratic_spline_integral(quadratic_spline *s, double xx);  // evaluates the integral of the prebuilt spline from x[0] to xx
void quadratic_spline_free(quadratic_spline *s); // free memory allocated in quadratic_spline_alloc

// cubic spline
typedef struct {int n; double *x, *y, *b, *c, *d;} cubic_spline;
cubic_spline * cubic_spline_alloc(int n, double *x, double *y);
double cubic_spline_evaluate(cubic_spline *s, double xx);
void cubic_spline_free(cubic_spline *s);

#define SPLINE //the header file is included and will not be included again
#endif