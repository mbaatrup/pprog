#include"header.h"
#define FMT "%.3e"
#define FMT_PLOT "%8f"

double fit_function(double t, gsl_vector *p){
	double A = gsl_vector_get(p,0);
	double T = gsl_vector_get(p,1);
	double B = gsl_vector_get(p,2);
	return A *exp(- t /T) + B;
}

double fit(gsl_vector *p, gsl_vector* dF) {
	double A = gsl_vector_get(p,0);
	double T = gsl_vector_get(p,1);
	double B = gsl_vector_get(p,2);
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double dy[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
	double F = 0, dFdA = 0, dFdT = 0, dFdB = 0, common_factor;
	for(int i = 0; i < N; i++){
		common_factor = 2 *(A *exp(-t[i]/T) + B - y[i])/ (dy[i]*dy[i]);
		dFdA += exp(- t[i]/T) *common_factor;
		dFdT += A *t[i] / (T*T) *exp(- t[i] /T) *common_factor;
		dFdB += common_factor;
		F += pow( (A*exp(- t[i] /T) + B - y[i]) /dy[i], 2); // master function to minimize: F(A,T,B)=∑i(f(ti)-yi)²/σi²
	}
	gsl_vector_set(dF,0,dFdA);
	gsl_vector_set(dF,1,dFdT);
	gsl_vector_set(dF,2,dFdB);
	return F;
}

int main(){ 
	// data
	double t_data[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y_data[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double dy_data[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};

	// allocate memory
	int n = 2, N = sizeof(t_data)/sizeof(t_data[0]);
	gsl_vector* x = gsl_vector_alloc(n); // coordinate vector
	gsl_vector* df = gsl_vector_alloc(n); // gradient vector
	gsl_matrix* H = gsl_matrix_alloc(n,n); // hessian matrix
	gsl_vector* t = gsl_vector_alloc(N); // time
	gsl_vector* y = gsl_vector_alloc(N); // activity
	gsl_vector* dy = gsl_vector_alloc(N); // error
	gsl_vector* dF = gsl_vector_alloc(3); // gradient vector
	gsl_vector* p = gsl_vector_alloc(3); // parameters

	// fill vectors with data
	for (int i = 0; i < N; i++){
		gsl_vector_set(t,i,t_data[i]);
		gsl_vector_set(y,i,y_data[i]);
		gsl_vector_set(dy,i,dy_data[i]);
	}
	// accuracy goal
	double eps = 1e-3;	

	printf("-----------------------------------------------------------------------\n");
	printf("Rosenbrock valley function: f(x,y) = (1 - x)² + 100*(y - x²)²");
	printf("\n-----------------------------------------------------------------------\n");
	double x0_rosenbrock = 0, y0_rosenbrock = 0; // start point

	printf("\nNewton's method for minimization\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	double fx = rosenbrock_with_hessian(x, df, H); // before minimization
	int steps = newton_min(rosenbrock_with_hessian, x, eps);
	fx = rosenbrock_with_hessian(x, df, H); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\nQuasi-Newton method with Broyden's update\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	fx = rosenbrock(x, df); // before minimization
	steps = quasi_newton_min(rosenbrock, x, eps);
	fx = rosenbrock(x, df); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\nNewton's method for root finding\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	fx = rosenbrock(x, df); // before minimization
	steps = newton_root(rosenbrock, x, eps);
	fx = rosenbrock(x, df); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\n-----------------------------------------------------------------------\n");
	printf("Himmelblau's function: f(x,y) = (x² + y - 11)² + (x + y² - 7)²");
	printf("\n-----------------------------------------------------------------------\n");
	double x0_himmelblau = -3.0, y0_himmelblau = -3.0; // start point

	printf("\nNewton's method for minimization\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	fx = himmelblau_with_hessian(x, df, H); // before minimization
	steps = newton_min(himmelblau_with_hessian, x, eps);
	fx = himmelblau_with_hessian(x, df, H); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\nQuasi-Newton method with Broyden's update\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	fx = himmelblau(x, df); // before minimization
	steps = quasi_newton_min(himmelblau, x, eps);
	fx = himmelblau(x, df); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\nNewton's method for root finding\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	fx = himmelblau(x, df); // before minimization
	steps = newton_root(himmelblau, x, eps);
	fx = himmelblau(x, df); // after minimization
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Minimum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Function value: f(x,y) = "FMT"\n", fx);
	printf("Gradient: (dfdx,dfdy) = ("FMT","FMT")\n", gsl_vector_get(df,0), gsl_vector_get(df,1));

	printf("\n-----------------------------------------------------------------------\n");
	printf("Non-linear least-squares fitting using quasi-Newton minimization with Broydens update");
	printf("\nFit function: f(t) = A*exp(-t/T) + B");
	printf("\nFunction to be minimized: F(A,T,B) = ∑_i[(f(t_i) - y_i)²/dy_i²]");
	printf("\n-----------------------------------------------------------------------\n");
	double A0 = 3.0, T0 = 3.0, B0 = 1.0; // start point
	gsl_vector_set(p,0, A0);
	gsl_vector_set(p,1, T0);
	gsl_vector_set(p,2, B0);
	steps = quasi_newton_min(fit, p, eps); 
	fit(p, dF); // minimizing the master function F
	printf("Steps: %i\n",steps);
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start parameters: A0 = %g, T0 = %g, B0 = %g\n", A0, T0, B0);
	printf("Final parameters: \n");
	printf("\tInitial amount: A = "FMT"\n", gsl_vector_get(p,0));
	printf("\tLifetime: T = "FMT"\n", gsl_vector_get(p,1));
	printf("\tBackground noise: B = "FMT"\n", gsl_vector_get(p,2));
	printf("Final gradient: (dFdA,dFdT,dFdB) = ("FMT","FMT","FMT")\n", gsl_vector_get(dF,0), gsl_vector_get(dF,1), gsl_vector_get(dF,2));

	// data for plot
	fprintf(stderr,"t \ty \tdy \n"); 
	for(int i = 0; i < N; i++){
		fprintf(stderr,""FMT_PLOT" \t"FMT_PLOT" \t"FMT_PLOT" \n", gsl_vector_get(t,i), gsl_vector_get(y,i), gsl_vector_get(dy,i));
	}
	fprintf(stderr,"\n\n");
	fprintf(stderr,"t \tf \n"); 
	for(double t = 0.0; t < 10.0; t += 1e-2){
		double ft = fit_function(t, p);
		fprintf(stderr,""FMT_PLOT" \t"FMT_PLOT" \n", t, ft);
	}

// free memory
	gsl_vector_free(x);
	gsl_vector_free(dF);
	gsl_matrix_free(H);
	gsl_vector_free(t);
	gsl_vector_free(y);
	gsl_vector_free(dy);
	gsl_vector_free(df);
	gsl_vector_free(p);

	return 0;
}