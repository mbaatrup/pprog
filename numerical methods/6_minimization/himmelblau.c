#include"header.h"

double himmelblau(gsl_vector* coordinates, gsl_vector* gradient){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	double fx = (x*x + y - 11)*(x*x + y - 11) + (x + y*y - 7)*(x + y*y - 7);
	gsl_vector_set(gradient,0, 4*x*(x*x + y - 11) + 2*(x + y*y - 7) ); // df/dx
	gsl_vector_set(gradient,1, 2*(x*x + y - 11) + 4*y*(x + y*y - 7) ); // df/dy
	return fx;
}

double himmelblau_with_hessian(gsl_vector* coordinates, gsl_vector* gradient, gsl_matrix* hessian){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	double fx = (x*x + y - 11)*(x*x + y - 11) + (x + y*y - 7)*(x + y*y - 7);
	gsl_vector_set(gradient,0, 4*x*(x*x + y - 11) + 2*(x + y*y - 7) ); // df/dx
	gsl_vector_set(gradient,1, 2*(x*x + y - 11) + 4*y*(x + y*y - 7) ); // df/dy
	gsl_matrix_set(hessian,0,0, 12*x*x + 4*y - 42 ); // d²f/dx²
	gsl_matrix_set(hessian,0,1, 4*x + 4*y ); // d²f/dxdy
	gsl_matrix_set(hessian,1,0, 4*x + 4*y ); // d²f/dydx
	gsl_matrix_set(hessian,1,1, 4*x + 12*y*y - 26 ); // d²f/dy²
	return fx;
}