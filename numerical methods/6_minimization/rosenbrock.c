#include"header.h"

double rosenbrock(gsl_vector* coordinates, gsl_vector* gradient){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	double fx = (1 - x)*(1 - x) + 100* (y-x*x)*(y-x*x);
	gsl_vector_set(gradient,0, -2*(1 - x) - 400*(y - x*x)*x ); // df/dx
	gsl_vector_set(gradient,1, 200*(y - x*x) ); // df/dy
	return fx;
}

double rosenbrock_with_hessian(gsl_vector* coordinates, gsl_vector* gradient, gsl_matrix* hessian){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	double fx = (1 - x)*(1 - x) + 100* (y-x*x)*(y-x*x);
	gsl_vector_set(gradient,0, -2*(1 - x) - 400*(y - x*x)*x ); // df/dx
	gsl_vector_set(gradient,1, 200*(y - x*x) ); // df/dy
	gsl_matrix_set(hessian,0,0, 2 + 1200*x*x - 400*y); // d²f/dx²
	gsl_matrix_set(hessian,0,1, - 400*x); // d²f/dxdy
	gsl_matrix_set(hessian,1,0, - 400*x); // d²f/dydx
	gsl_matrix_set(hessian,1,1, 200); // d²f/dy²
	return fx;
}