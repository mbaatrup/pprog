#include"header.h"

int newton_min(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double eps){
// input function syntax: f(gsl_vector* x, gsl_vector* df, gsl_matrix* H);
// x is changed from the initial guess to the minimum point
	int steps = 0, steps_max = 1000, n = x->size;
	// allocate memory
	gsl_vector* xx = gsl_vector_alloc(n); // copy of coordinate vector
	gsl_vector* df = gsl_vector_alloc(n); // gradient vector
	gsl_vector* dff = gsl_vector_alloc(n); // copy of gradient vector
	gsl_vector* Dx = gsl_vector_alloc(n); // step vector
	gsl_matrix* H = gsl_matrix_alloc(n,n); // hessian matrix
	gsl_matrix* R = gsl_matrix_alloc(n,n); // right-triangular matrix for QR-decomposition
	gsl_matrix* Q = gsl_matrix_alloc(n,n); // unitary matrix for QR-decomposition	
	// taking steps
	do{ 
		steps++;
		// fill df and H at position x
		double fx = f(x,df,H); 
		// Newton's step: solve H*Dx = -df for Dx through QR decomposition of H
		gsl_matrix_memcpy(Q,H);
		qr_decomposition(Q,R); 
		qr_solve(Q,R,df,Dx);
		gsl_vector_scale(Dx,-1); // Dx result
		// backtracking line search
		double lambda = 1, lambda_min = 0.02, alpha = 1e-4;
		do{
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down

			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			double fxx = f(xx,dff,H); // fill dff, change H (not used in the following)

			double Dx_dot_df = 0; // Dx_dot_df <- Dx^T df
			for(int i = 0; i<n; i++){
				Dx_dot_df += gsl_vector_get(Dx,i)*gsl_vector_get(df,i);
			}
			if( fxx < fx + alpha*lambda*Dx_dot_df || lambda < lambda_min) break;	
			// stop if: f(x + lambda*Dx) <  f(x) + alpha*lambda*dotproduct(Dx,df) - this is the Armijo condition
			// OR if lambda is too small
			// otherwise continue:
			lambda /= 2.0; // lambda <- lambda/2
			gsl_vector_scale(Dx,0.5); // Dx <- lambda*Dx
		}while(1); // continue until break by if-statement
		// save new coordinates - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(df,dff); // df <- dff
	} while(gsl_blas_dnrm2(df) > eps && steps < steps_max); 
	// continue if: || grad(f) || > eps
	// AND not to many steps 
	// otherwise stop - no more steps
	if(steps == 1000){ // if too many steps
		printf("newton_min: did not converge\n");
	}
	// free memory
	gsl_vector_free(xx);
	gsl_vector_free(df);
	gsl_vector_free(dff);
	gsl_vector_free(Dx);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
	gsl_matrix_free(Q);

	return steps;
}

int quasi_newton_min(double f(gsl_vector* x, gsl_vector* df), gsl_vector* x, double eps){
	int steps = 0, steps_max = 1e6, n = x->size;
	// allocate memory
	gsl_vector* Dx = gsl_vector_alloc(n); // step vector
	gsl_vector* xx = gsl_vector_alloc(n); // position vector: xx = x + Dx
	gsl_vector* fxx = gsl_vector_alloc(n); // function value vector: fxx = f(x + Dx)
	gsl_vector* df = gsl_vector_alloc(n); // gradient vector: grad(f(x))
	gsl_vector* dff = gsl_vector_alloc(n); // gradient vector: grad(f(x + Dx))
	gsl_vector* y = gsl_vector_alloc(n); // gradient difference: dff - df
	gsl_matrix* H_inv = gsl_matrix_alloc(n,n); // inverse hessian matrix
	gsl_matrix_set_identity(H_inv); // set the inverse Hessian matrix to unity
	gsl_vector* v = gsl_vector_alloc(n);
	gsl_vector* w = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	// take steps
	do{ 
		steps++;
		// fill df at position x
		double fx = f(x,df); 
		// calculate Dx
		gsl_blas_dgemv(CblasNoTrans, -1.0, H_inv, df, 0, Dx); // Dx = -1.0 * H_inv * df
		// backtracking line search
		double lambda = 1, lambda_min = 0.02, alpha = 1e-4;
		do{
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down
			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			double fxx = f(xx,dff); // fill dff
			double Dx_dot_df = 0; // Dx_dot_df <- Dx^T df
			for(int i = 0; i<n; i++){
				Dx_dot_df += gsl_vector_get(Dx,i)*gsl_vector_get(df,i);
			}
			// stop if converged or diverged:
			if( fxx < fx + alpha*lambda*Dx_dot_df ) break; // converged (Armijo condition)
			if( lambda < lambda_min ){ // diverged
				gsl_matrix_set_identity(H_inv);
				break;
			}
			// otherwise continue:
			lambda /= 2.0; // lambda <- lambda/2
			gsl_vector_scale(Dx,0.5); // Dx <- lambda*Dx
		}while(1); // continue until break by if-statement
	// Broyden's update
		// generate y = dff - df
		gsl_vector_memcpy(y, dff);
		gsl_vector_sub(y, df);
		// numerator B
		gsl_blas_dgemv(CblasNoTrans, 1.0, H_inv, y, 0.0, v); // v = H_inv * y + 0.0 * v
		gsl_vector_memcpy(w, Dx);
		gsl_vector_sub(w, v);
		gsl_matrix_set_zero (A);
		gsl_blas_dger(1.0, w, Dx, A); // A = 1.0 w Dx^T + A
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, H_inv, 0.0, B); // B = 1.0 A * H_inv + 0.0 * B
		// denominator b
		gsl_blas_dgemv(CblasNoTrans, 1.0, H_inv, Dx, 0.0, v); // v = H_inv * Dx + 0.0 * v
		double b;
		gsl_blas_ddot(y,v,&b);
		// H_inv = H_inv + 1/b * B
		gsl_matrix_scale(B,1.0/b);
		gsl_matrix_add(H_inv,B); // updated H_inv
		// save new coordinates - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(df,dff); // df <- dff
	} while(gsl_blas_dnrm2(df) > eps && steps < steps_max); 
	// continue if: || grad(f) || > eps
	// AND not to many steps 
	// otherwise stop - no more steps
	if(steps == 1000){ // if too many steps
		printf("newton_min: did not converge\n");
	}
	// free memory
	gsl_vector_free(Dx);
	gsl_vector_free(xx);
	gsl_vector_free(fxx);
	gsl_vector_free(df);
	gsl_vector_free(dff);
	gsl_vector_free(y);
	gsl_matrix_free(H_inv);
	gsl_vector_free(v);
	gsl_vector_free(w);
	gsl_matrix_free(A);
	gsl_matrix_free(B);

	return steps;
}

int newton_root(double f(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double eps){
	int steps = 0;
	int function_calls = 0;
	double dx = 1e-6;
	int n = x->size;
	gsl_vector* fx = gsl_vector_alloc(n);
	f(x,fx);
	function_calls++;
	// allocate memory
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* xx = gsl_vector_alloc(n);
	gsl_vector* fxx = gsl_vector_alloc(n);
	do{ // take a step downhill
		// Jacobian matrix
		for(int j = 0; j < n; j++){
			// change xj to xj + dx
			gsl_vector_set(x,j, gsl_vector_get(x,j) + dx );
			// calculate finite difference: df = f(x1,...,xj + dx,...,xn) - f(x1,...,xn)
			f(x,df); // first: df = f(x1,...,xj + dx,...,xn)
			function_calls++;
			gsl_vector_sub(df,fx); // then: df = f(x1,...,xj + dx,...,xn) - f(x1,...,xn)
			// calculate partial derivatives and store in Jacobian matrix
			for(int i = 0; i < n; i++){
				gsl_matrix_set(J,i,j, gsl_vector_get(df,i) / dx );
			}
			// change xj + dx back to xj
			gsl_vector_set(x,j, gsl_vector_get(x,j) - dx );
		}
		// Newton's step: solve J*Dx = -fx for Dx through QR decomposition of J
		qr_decomposition(J,R); // remember, J is changed here
		qr_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1); // Dx result
		// backtracking line search
		double lambda = 1, lambda_min = 0.02;
		do{
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down
			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			f(xx,fxx); // fill fxx with function values
			function_calls++;
		 	// stop criteria:
			if(gsl_blas_dnrm2(fxx) < (1 - lambda/2)*gsl_blas_dnrm2(fx) || lambda < lambda_min) break;	
			// stop if ||f(x + lambda*Dx)|| < (1 - lambda/2) ||f(x)|| OR if lambda is too small
			// otherwise continue: modify lambda and thereby step size
			lambda /= 2.0; // lambda <- lambda/2
			gsl_vector_scale(Dx,0.5); // Dx <- Dx/2
			// continue criteria:
		}while(1);
		// save result - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(fx,fxx); // fx <- fxx
		steps++;
 	}while(gsl_blas_dnrm2(fx) > eps && gsl_blas_dnrm2(Dx) > dx); 
	// continue if not converged and stepsize not too small - otherwise stop
	// no more steps
	// free memory
	gsl_vector_free(fx);
	gsl_vector_free(df);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(Dx);
	gsl_vector_free(xx);
	gsl_vector_free(fxx);
	
	return steps;
}