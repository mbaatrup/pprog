#include"header.h"

void qr_decomposition(gsl_matrix* A, gsl_matrix* R){
// performs in-place modified Gram-Schmidt orthogonalization of an n×m (n≥m) matrix A: 
// A turns into Q and the square m×m matrix R is computed
	int m = A->size2;
	for(int j = 0; j < m; j++){
		// diagonal elements of R 
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
}

void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
// solves the equation QRx=b by applying Q^T to the vector b (saving the result in x) 
// and then performing in-place back-substitution on x
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x); // applying Q^T to b and saving the result in x
	// in-place back-substitution
	int n = x->size;
	for(int j = n - 1; j >= 0; j--){
		double sum = 0;
		for(int k = j + 1; k < n; k++){
			sum += gsl_matrix_get(R,j,k)*gsl_vector_get(x,k);
		}
		double x_j = (gsl_vector_get(x,j) - sum) / gsl_matrix_get(R,j,j);
		gsl_vector_set(x,j,x_j);
	}
}