#ifndef HEADER // if the header file is not already included, then include it
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

// vectors and matrices
void qr_decomposition(gsl_matrix* A, gsl_matrix* R);
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x); // solve linear equations QRx = b

// non-linear equations
double rosenbrock(gsl_vector* coordinates, gsl_vector* gradient);
double himmelblau(gsl_vector* coordinates, gsl_vector* gradient);
double rosenbrock_with_hessian(gsl_vector* coordinates, gsl_vector* gradient, gsl_matrix* hessian);
double himmelblau_with_hessian(gsl_vector* coordinates, gsl_vector* gradient, gsl_matrix* hessian);
void rosenbrock_root(gsl_vector* coordinates, gsl_vector* function_values);
void himmelblau_root(gsl_vector* coordinates, gsl_vector* function_values);

// minimization methods
int newton_min(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double eps);
int quasi_newton_min(double f(gsl_vector* x, gsl_vector* df), gsl_vector* x, double eps);
int newton_root(double f(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double eps);

#define HEADER // the header file is included and will not be included again
#endif