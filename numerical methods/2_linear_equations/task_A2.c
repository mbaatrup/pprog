#include"header.h"

void task_A2(int n){
	printf("\nTASK A2: Solve linear equation\n\n");
// allocate memory to matrices and vectors
	gsl_matrix *A = gsl_matrix_calloc(n,n);
	gsl_matrix *Q = gsl_matrix_calloc(n,n);
	gsl_matrix *R = gsl_matrix_calloc(n,n);
	gsl_vector *x = gsl_vector_calloc(n);
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector *b_test = gsl_vector_calloc(n);

// generate matrix A and vector b
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,RANDOM_NUMBER);
		for (int j = 0; j < n; j++){
			gsl_matrix_set(A,i,j,RANDOM_NUMBER);
		}
	}
	printf("Random square matrix: \nA = ");
	print_matrix(A);
	printf("Random vector: \nb = ");
	print_vector(b);

// QR decomposition
	gsl_matrix_memcpy(Q,A);
	qr_gs_decomp(Q,R);
	printf("QR decomposition: \nQ = ");
	print_matrix(Q);
	printf("R = ");
	print_matrix(R);

// solve linear equation QRx = b
	qr_gs_solve(Q, R, b, x);
	printf("Solve linear equation QRx = b: \nx =");	
	print_vector(x);

	// test solution
	gsl_blas_dgemv(CblasNoTrans, 1.0, A, x, 0.0, b_test); // applying A to x and writing the result in b_test
	printf("Ax = ");
	print_vector(b_test);
	printf("b = ");
	print_vector(b);
	printf("Ax = b, as it should be\n");

// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(b_test);
}
