#include"function_declarations.h"

void random_matrix(gsl_matrix* M){
	for(int i = 0; i < M->size1; i++){
		for (int j = 0; j < M->size2; j++){
			gsl_matrix_set(M,i,j,rand()/RAND_MAX);
		}
	}
}