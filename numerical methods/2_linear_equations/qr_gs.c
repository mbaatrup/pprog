#include"header.h"

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
// performs in-place modified Gram-Schmidt orthogonalization of an n×m (n ≥ m) matrix A: 
// A turns into Q and the square m×m matrix R is computed
	int m = A->size2;
	for(int j = 0; j < m; j++){
		// diagonal elements of R
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
}

void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
// solves the equation QRx = b by applying Q^T to the vector b (saving the result in x) 
// and then performing in-place back-substitution on x
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x); // applying Q^T to b and saving the result in x
	back_substitution(R, x);
}

void back_substitution(gsl_matrix* R, gsl_vector* x){
	int m = x->size;
	for(int i = m - 1; i >= 0; i--){
		double sum = 0;
		for(int k = i + 1; k < m; k++){
			sum += gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
		}
		double xi = (gsl_vector_get(x,i) - sum) / gsl_matrix_get(R,i,i);
		gsl_vector_set(x,i,xi);
	}
}

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv){ 
// calculates the inverse of the matrix A and saves the result in A_inv
	int n = Q->size1;
	gsl_vector *x  = gsl_vector_calloc(n);
	gsl_vector *b  = gsl_vector_calloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_gs_solve(Q,R,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
	}
	gsl_vector_free(x);
	gsl_vector_free(b);
}
