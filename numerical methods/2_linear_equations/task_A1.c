#include"header.h"

void task_A1(int n, int m){ 
	printf("Task A1: Perform qr decomposition\n");
// allocate memory to matrices A and R
	gsl_matrix *A = gsl_matrix_calloc(n,m);
	gsl_matrix *R = gsl_matrix_calloc(m,m);
	gsl_matrix* qtq = gsl_matrix_calloc(m,m);
	gsl_matrix* qr = gsl_matrix_calloc(n,m);

// generate matrix A
	for(int i = 0; i < n; i++){
		for (int j = 0; j < m; j++){
			gsl_matrix_set(A,i,j,RANDOM_NUMBER);
		}
	}
	printf("\nRandom matrix: \nA = ");
	print_matrix(A);

// QR decomposition
	qr_gs_decomp(A,R);

	printf("QR decomposition: \nQ = ");
	print_matrix(A);
	printf("R = ");
	print_matrix(R);
	printf("R is upper triangular\n");

	// Is Q^T Q = I?
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,1.0, A, A, 0.0, qtq); 
	// syntax: gsl_blas_dgemm(TransA, TransB, alpha, A, B, beta, C): C = \alpha op(A) op(B) + \beta C
	printf("Q^T Q = ");
	print_matrix(qtq);
	printf("Q is orthogonal\n");

	// Is QR = A?
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0, A, R, 0.0, qr); 
	printf("QR = ");
	print_matrix(qr);
	printf("= A, as it should be\n");

// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(qtq);
	gsl_matrix_free(qr);
}
