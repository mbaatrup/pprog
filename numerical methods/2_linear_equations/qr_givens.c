#include"header.h"

void qr_givens_decomp(gsl_matrix* A){
// calculates R and stores the t-values in the zeroed elements
	int n = A->size1, m = A->size2;
	for(int q = 0; q < m; q++){
		for(int p = q + 1; p < n; p++){
			double t = atan2(gsl_matrix_get(A,p,q), gsl_matrix_get(A,q,q));
			double s = sin(t), c = cos(t);
			for(int k = q; k < m; k++){
				double xq = gsl_matrix_get(A,q,k);
				double xp = gsl_matrix_get(A,p,k);
				gsl_matrix_set(A,q,k, xq*c+ xp*s);
				gsl_matrix_set(A,p,k, -xq*s + xp*c);
			}
			gsl_matrix_set(A,p,q, t);
		}
	}
}

void qr_givens_Q(gsl_matrix* QR, gsl_matrix* Q){
// calculates Q by applying Q^T [mxn] to basis vectors ei [n]
	int n = QR->size1, m = QR->size2;
	gsl_vector* ei = gsl_vector_alloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set_basis(ei,i); // all elements zero except the ith element which is one
		qr_givens_QT(QR,ei); // Q^T ei <- ei
		for(int j = 0; j < m; j++){
			gsl_matrix_set(Q,i,j, gsl_vector_get(ei,j));
		}
	}
	gsl_vector_free(ei);
}

void qr_givens_R(gsl_matrix* QR, gsl_matrix* R){
// extract R from QR
	assert(R->size1 == R->size2); // R should be square
	assert(QR->size2 == R->size2); // R should be [mxm]
	int m = R->size1;
	for(int j = 0; j < m; j++){ // columns
		for(int i = 0; i < m; i++){ // rows
			if(j >= i) gsl_matrix_set(R,i,j, gsl_matrix_get(QR,i,j));
			else gsl_matrix_set(R,i,j, 0); // upper triangular
		}
	}
}

void qr_givens_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x){
// solves Rx = Q^T b by calculating Q^T b and doing back-substitution
// the result is stored in x
	gsl_vector* bb = gsl_vector_alloc(b->size);
	gsl_vector_memcpy(bb,b); // avoid changing original b

	qr_givens_QT(QR,bb); // only the t-values in QR are used (not R)
	int m = x->size;
	for(int i = 0; i < m; i++){
		gsl_vector_set(x,i, gsl_vector_get(bb,i)); // b[n], x[m]
	}
	back_substitution(QR,x); // only the R-part of QR is used (not the t-values)

	gsl_vector_free(bb);
}

void qr_givens_QT(gsl_matrix* QR, gsl_vector* v){
// extracts t from QR and applies Q^T [mxn] to v [n] (using t)
// Q^T v <- v
	int n = QR->size1, m = QR->size2;
	for(int q = 0; q < m; q++){
		for(int p = q + 1; p < n; p++){
			double t = gsl_matrix_get(QR,p,q);
			double vq = gsl_vector_get(v,q);
			double vp = gsl_vector_get(v,p);
			gsl_vector_set(v,q, vq*cos(t) + vp*sin(t));
			gsl_vector_set(v,p, -vq*sin(t) + vp*cos(t));
		}
	}
}

void qr_givens_inverse(gsl_matrix* QR, gsl_matrix* A_inv){ 
// calculates the inverse of the matrix A and stores the result in A_inv
	int n = QR->size1;
	gsl_vector *x  = gsl_vector_calloc(n);
	gsl_vector *b  = gsl_vector_calloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_givens_solve(QR,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
	}
	gsl_vector_free(x);
	gsl_vector_free(b);
}
