#ifndef HEADER // if the header file is not already included, then include it
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#define RANDOM_NUMBER ((double) rand()/RAND_MAX) // random number from 0 to 1
#define EPSABS 1e-3
#define EPSREL 1e-1

// tasks
void task_A1(int n, int m);
void task_A2(int n);
void task_B(int n);
void task_C(int n, int m);

// vectors and matrices
void random_matrix(gsl_matrix* M);
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);

// Gram-Schmidt orthogonalization (qr_gs.c)
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R); // QR decomposition
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x); // solve linear equal QRx = b
void back_substitution(gsl_matrix* R, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv); // calculate the inverse of matrix A = QR and save the result in A_inv

// Givens rotation (qr_givens.c)
void qr_givens_decomp(gsl_matrix* A);
void qr_givens_Q(gsl_matrix* QR, gsl_matrix* Q);
void qr_givens_R(gsl_matrix* QR, gsl_matrix* R);
void qr_givens_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x);
void qr_givens_QT(gsl_matrix* QR, gsl_vector* v);

#define HEADER //the header file is included and will not be included again 
#endif