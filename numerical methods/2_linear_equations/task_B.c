#include"header.h"

void task_B(int n){
	printf("\nTASK B: Matrix inverse by Gram-Schmidt QR factorization\n\n");
// allocate memory to matrices and vectors
	gsl_matrix *A = gsl_matrix_calloc(n,n);
	gsl_matrix *Q = gsl_matrix_calloc(n,n);
	gsl_matrix *R = gsl_matrix_calloc(n,n);
	gsl_matrix *A_inv = gsl_matrix_calloc(n,n);

// generate matrix A
	for(int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(A,i,j,RANDOM_NUMBER);
		}
	}
	printf("Random square matrix: \nA = ");
	print_matrix(A);

// QR decomposition
	gsl_matrix_memcpy(Q,A); // makes a copy of A and call it Q, because qr_gs_decomp alters A to make Q
	qr_gs_decomp(Q,R);

	printf("QR decomposition: \nQ = ");
	print_matrix(Q);
	printf("R = ");
	print_matrix(R);

// inverse of A
	qr_gs_inverse(Q,R,A_inv);
	printf("Matrix inverse: \nA_inv = ");
	print_matrix(A_inv);

// test result	
	gsl_matrix* AA_inv = gsl_matrix_calloc(n,n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, A_inv, 0.0, AA_inv); 
	// syntax: gsl_blas_dgemm(TransA, TransB, alpha, A, B, beta, C): C = \alpha op(A) op(B) + \beta C
	printf("A A_inv = ");
	print_matrix(AA_inv);
	printf("= I (the identity matrix), as it should be\n");

// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(A_inv);
	gsl_matrix_free(AA_inv);
}
