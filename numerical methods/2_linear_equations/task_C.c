#include"header.h"

void task_C(int n, int m){
	printf("\nTASK C: QR decomposition by Givens rotations\n\n");

// allocate memory
	gsl_matrix* A = gsl_matrix_calloc(n,m); // Ax = b
	gsl_vector* x = gsl_vector_calloc(m);
	gsl_vector* b = gsl_vector_calloc(n);
	gsl_vector* b_test = gsl_vector_calloc(n);
	gsl_matrix* Q = gsl_matrix_calloc(n,m); // orthogonal matrix
	gsl_matrix* R = gsl_matrix_calloc(m,m); // upper triangular matrix
	gsl_matrix* QR = gsl_matrix_calloc(n,m); // contains R and has t-values in place of zeroed elements
	gsl_matrix* qtq = gsl_matrix_calloc(m,m); // marix product: Q^T Q
	gsl_matrix* qr = gsl_matrix_calloc(n,m); // matrix product: Q R
	gsl_matrix* A_inv = gsl_matrix_calloc(n,n); // matrix inverse: A^{-1}
	gsl_matrix* AA_inv = gsl_matrix_calloc(n,n); // matrix product: A A^{-1}
	

// generate matrix A and vector b
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i, RANDOM_NUMBER);
		for (int j = 0; j < m; j++){
			gsl_matrix_set(A,i,j, RANDOM_NUMBER);
		}
	}
	printf("Random matrix: \nA = ");
	print_matrix(A);
	printf("Random vector: \nb = ");
	print_vector(b);

// QR decomposition
	gsl_matrix_memcpy(QR,A);
	qr_givens_decomp(QR);
	qr_givens_Q(QR,Q);
	printf("QR decomposition: \nQ = ");
	print_matrix(Q);
	qr_givens_R(QR,R);
	printf("R = ");
	print_matrix(R);

	// Is Q^T Q = I?
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,1.0, Q, Q, 0.0, qtq); 
	// syntax: gsl_blas_dgemm(TransA, TransB, alpha, A, B, beta, C): C = \alpha op(A) op(B) + \beta C
	printf("Q^T Q = ");
	print_matrix(qtq);

	// Is QR = A?
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0, Q, R, 0.0, qr); 
	printf("QR = ");
	print_matrix(qr);
	printf("= A, as it should be\n");

// solve linear equation QRx = b
	qr_givens_solve(QR,b,x);
	printf("Solve linear equation QRx = b: \nx =");	
	print_vector(x);

	// test solution
	gsl_blas_dgemv(CblasNoTrans, 1.0, A, x, 0.0, b_test); // applying A to x and writing the result in b_test
	printf("Ax = ");
	print_vector(b_test);
	printf("b = ");
	print_vector(b);
	printf("Ax = b, as it should be\n");

// inverse of A
	qr_givens_inverse(QR,A_inv);
	printf("Matrix inverse: \nA_inv = ");
	print_matrix(A_inv);

	// test result	
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, A_inv, 0.0, AA_inv); 
	// syntax: gsl_blas_dgemm(TransA, TransB, alpha, A, B, beta, C): C = \alpha op(A) op(B) + \beta C
	printf("A A_inv = ");
	print_matrix(AA_inv);
	printf("= I (the identity matrix), as it should be\n");

// free memory
	gsl_matrix_free(A);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(b_test);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(QR);
	gsl_matrix_free(qtq);
	gsl_matrix_free(qr);
	gsl_matrix_free(A_inv);
	gsl_matrix_free(AA_inv);
}
