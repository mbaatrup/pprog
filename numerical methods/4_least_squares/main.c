#include"header.h"

// fit functions
double fit_functions(int i, double x){
	switch(i){
		case 0: return log(x); break;
   		case 1: return 1.0; break;
 		case 2: return x; break;
		default: return NAN;
  	}
}
double fit(int m, double x, gsl_vector* c){
	double yy = 0;
	for(int i = 0; i < m; i++){
		yy += gsl_vector_get(c,i)*fit_functions(i,x);
	}
	return yy;
}
double fit_plus(int m, int i, double x, gsl_vector* c, gsl_vector* dc){
	return fit(m,x,c) + gsl_vector_get(dc,i)*fit_functions(i,x);
}
double fit_minus(int m, int i, double x, gsl_vector* c, gsl_vector* dc){
	return fit(m,x,c) - gsl_vector_get(dc,i)*fit_functions(i,x);
}

int main(){ 
	// data
	double x_data[]  = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
	double y_data[]  = {-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy_data[] = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};

	int n = sizeof(x_data)/sizeof(x_data[0]); // number of data points
	int nn = 1e2; // number of points for plotting the fit
	int m = 3; // number of fit functions

	// allocate memory
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_vector* dc = gsl_vector_alloc(m);

	for (int i = 0; i < n; i++){
		gsl_vector_set(x,i,x_data[i]);
		gsl_vector_set(y,i,y_data[i]);
		gsl_vector_set(dy,i,dy_data[i]);
	}

	// data to stdout
	printf("x \ty \tdy \n"); 
	for(int i = 0; i < n; i++){
		printf(""FMT" \t"FMT" \t"FMT" \n",
			gsl_vector_get(x,i), gsl_vector_get(y,i), gsl_vector_get(dy,i)); 
	}
	printf("\n\n");

	// fit
	printf("Least squares fit via QR decomposition\n");
	qr_least_squares_fit(m, fit_functions, x, y, dy, c, dc);

	printf("c \tdc \ti (fit function number)\n");
	for (int i = 0; i < m; i++){
		printf(""FMT" \t"FMT" \t%i \n", gsl_vector_get(c,i),gsl_vector_get(dc,i), i);
	}
	printf("\n\n");

	double xx; // points for plotting the fit
	double dxx = (gsl_vector_get(x, n - 1) - gsl_vector_get(x, 0))/(nn - 1); // step size
	
	printf("xx \tyy \n");
	for(int j = 0; j < nn; j++){
			xx = gsl_vector_get(x,0) + j*dxx;
			printf(""FMT" \t"FMT"\n", xx, fit(m, xx, c));
		}
	printf("\n\n");

	printf("xx \tyy + dyy \tyy - dyy \ti (fit function number)\n");
	for(int i = 0; i < m; i++){
		for(int j = 0; j < nn; j++){
			xx = gsl_vector_get(x,0) + j*dxx;
			printf(""FMT" \t"FMT" \t"FMT" \t%i \n", xx, fit_plus(m, i, xx, c, dc), fit_minus(m, i, xx, c, dc), i);
		}
		printf("\n\n");
	}

	// data to stderr
	fprintf(stderr,"x \ty \tdy \n"); 
	for(int i = 0; i < n; i++){
		fprintf(stderr,""FMT" \t"FMT" \t"FMT" \n",
			gsl_vector_get(x,i), gsl_vector_get(y,i), gsl_vector_get(dy,i));
	}
	fprintf(stderr,"\n\n");

	// fit
	fprintf(stderr,"Least squares fit via singular value decomposition\n");
	gsl_vector_set_zero(c);
	gsl_vector_set_zero(dc);
	sv_least_squares_fit(m,fit_functions, x, y, dy, c);

	fprintf(stderr,"\nc \ti (fit function number)\n");
	for (int i = 0; i < m; i++){
		fprintf(stderr,""FMT" \t%i \n", gsl_vector_get(c,i), i);
	}
	fprintf(stderr,"\n\n");
	
	fprintf(stderr,"xx \tyy \n");
	for(int j = 0; j < nn; j++){
			xx = gsl_vector_get(x,0) + j*dxx;
			fprintf(stderr,""FMT" \t"FMT"\n", xx, fit(m, xx, c));
		}
	fprintf(stderr,"\n\n");

	// free memory 
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_vector_free(dy);
	gsl_vector_free(c);
	gsl_vector_free(dc);

	return 0;
}
