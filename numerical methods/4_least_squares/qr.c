#include"header.h"

void qr_decomposition(gsl_matrix* A, gsl_matrix* R){
	// performs in-place modified Gram-Schmidt orthogonalization of an n×m (n≥m) matrix A: 
	// A turns into Q and the square m×m matrix R is computed
	int m = A->size2;
	for(int j = 0; j < m; j++){
		// diagonal elements of R
		gsl_vector_view a = gsl_matrix_column(A,j); // a is a column vector of matrix A
		double a_norm = gsl_blas_dnrm2(&a.vector); // Euclidean norm of vector a
		gsl_matrix_set(R,j,j,a_norm); // diagonal elements of R matrix (a norm = u norm)
		gsl_vector_scale(&a.vector,1/a_norm); // a is normalized (u vector)
		for(int jj = j + 1; jj < m; jj++){
			gsl_vector_view aa = gsl_matrix_column(A,jj);
			double proj = 0; gsl_blas_ddot(&a.vector,&aa.vector,&proj); // inner product of a and aa
			gsl_blas_daxpy(-proj,&a.vector,&aa.vector); // aa -> aa - proj*a
			gsl_matrix_set(R,j,jj,proj); // R elements above diagonal: projections
			gsl_matrix_set(R,jj,j,0); // R elements below diagonal: zeros
		}
	}
}

void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
	// solves the equation QRx = b for x
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	back_substitution(R, x);
}

void back_substitution(gsl_matrix* R, gsl_vector* x){
	int m = x->size;
	for(int i = m - 1; i >= 0; i--){
		double sum = 0;
		for(int k = i + 1; k < m; k++){
			sum += gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
		}
		double xi = (gsl_vector_get(x,i) - sum) / gsl_matrix_get(R,i,i);
		gsl_vector_set(x,i,xi);
	}
}

void qr_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv){
	// calculates the inverse of the matrix A and saves the result in A_inv
	int n = Q->size1;
	// allocate memory
	gsl_vector *x  = gsl_vector_calloc(n);
	gsl_vector *b  = gsl_vector_calloc(n);
	// calculate the inverse of matrix A
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,1.0); // b is now a unit vector
		qr_solve(Q,R,b,x); // solves the equation QRx = b for x
		gsl_vector_set(b,i,0.0); // b is a vector of zeros
		gsl_matrix_set_col(A_inv,i,x); // the x solution is saved as a column in A_inv
	}
	// free memory
	gsl_vector_free(x);
	gsl_vector_free(b);
}

void qr_least_squares_fit(int m, double fit_functions(int j, double x), 
	gsl_vector* x, gsl_vector* y, gsl_vector* dy,
	gsl_vector* c, gsl_vector* dc){
	int n = x->size;
	// allocate memory
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(m,m); // right triangular matrix
	gsl_matrix* R_inv = gsl_matrix_alloc(m,m);
	gsl_matrix* I = gsl_matrix_alloc(m,m);
	gsl_matrix_set_identity(I);
	gsl_matrix* C = gsl_matrix_alloc(m,m); // covariance matrix
	// building A and b 
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
		for(int j = 0; j < m; j++){
			gsl_matrix_set(A,i,j, fit_functions(j,gsl_vector_get(x,i)) / gsl_vector_get(dy,i) );
		}
	}
	// QR decomposition (in-place modified Gram-Schmidt orthogonalization)
	qr_decomposition(A, R); // A -> Q and R is build
	// coefficients
	qr_solve(A, R, b, c); // solves the equation QRc = b for c
	// covariance matrix and coefficient errors
	qr_inverse(I, R, R_inv); // calculates the inverse of matrix R and saves the result in R_inv
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, R_inv, R_inv, 0, C);
	for (int i = 0; i < m; i++){
		gsl_vector_set(dc,i, sqrt(gsl_matrix_get(C,i,i)));
	}
	// free memory
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(R);
	gsl_matrix_free(R_inv);
	gsl_matrix_free(I);
	gsl_matrix_free(C);
}
