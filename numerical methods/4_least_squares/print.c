#include"header.h"

void print_vector(gsl_vector *v){
	printf("\n");
	for(int element = 0; element < v->size; element++){
			printf(FMT, gsl_vector_get(v,element));
			printf("\n");
		}
}

void print_matrix(gsl_matrix *M){
	printf("\n");
	for(int row = 0; row < M->size1; row++){
		for (int column = 0; column < M->size2; column++){
			printf(FMT, gsl_matrix_get(M,row,column));
		}
		printf("\n");
	}
}

void fprint_matrix(gsl_matrix *M){
	fprintf(stderr,"\n");
	for(int row = 0; row < M->size1; row++){
		for (int column = 0; column < M->size2; column++){
			fprintf(stderr,FMT, gsl_matrix_get(M,row,column));
		}
		fprintf(stderr,"\n");
	}
}
