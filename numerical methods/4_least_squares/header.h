#ifndef HEADER // if the header file is not already included, then include it
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#define EPSABS 1e-3
#define EPSREL 1e-1
#define FMT "%7.3g"

// vectors and matrices
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);
void fprint_matrix(gsl_matrix *M);
int matrix_diagonalization(gsl_matrix* A, gsl_matrix* V, gsl_matrix* D);
int jacobi_rotation(gsl_matrix* A, gsl_matrix* V, gsl_matrix* D, int p, int q);

// ordinary least squares solution via QR decomposition
void qr_decomposition(gsl_matrix* A, gsl_matrix* R);
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x); // solve linear equal QRx = b
void back_substitution(gsl_matrix* R, gsl_vector* x); // in-place back-substitution
void qr_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* A_inv); // calculates the inverse of matrix A = QR and saves the result in A_inv
void qr_least_squares_fit(int m, double fit_functions(int j, double x), 
	gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c, gsl_vector* dc);

// ordinary least squares solution via singular value decomposition
void sv_decomposition(gsl_matrix* A, gsl_matrix* U, gsl_matrix* S, gsl_matrix* V); // builds U, S, and V from A
void sv_solve(gsl_matrix* U, gsl_matrix* S, gsl_matrix* V, gsl_vector* b, gsl_vector* c); // solves S V^T c = U^T b for c
void sv_least_squares_fit(int m, double fit_functions(int j, double x), 
	gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c);


#define HEADER //the header file is included and will not be included again
#endif