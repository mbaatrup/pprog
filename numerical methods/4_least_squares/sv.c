#include"header.h"

void sv_decomposition(gsl_matrix* A, gsl_matrix* U, gsl_matrix* S, gsl_matrix* V){
	// builds U, S, and V from A
	int n = A->size1, m = A->size2;
	// allocate memory
	gsl_matrix* ATA = gsl_matrix_alloc(m,m);
	gsl_matrix* D = gsl_matrix_alloc(m,m);
	gsl_matrix* AV = gsl_matrix_alloc(n,m);
	gsl_matrix* US = gsl_matrix_alloc(n,m);
	gsl_matrix* USVT = gsl_matrix_alloc(n,m);
	// build A^T A and diagonalize it - building V and D
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0, ATA);
	gsl_matrix_set_identity(D);
	matrix_diagonalization(ATA, V, D);
	// build S and U
	gsl_matrix_set_identity(S);
	for(int i = 0; i < m; i++){
		double di = gsl_matrix_get(D,i,i); // diagonal element number i
		gsl_matrix_set(S,i,i, sqrt(di)); // S -> D⁰⁵
		gsl_matrix_set(D,i,i, 1.0/sqrt(di)); // D -> D⁻⁰⁵
	}
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, V, 0, AV); // AV = A V
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, AV, D, 0, U); // U = A V D⁻⁰⁵
	// test decomposition
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, U, S, 0, US); // US = US
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, US, V, 0, USVT); // USVT = U S V^T
	fprintf(stderr,"\nA = ");
	fprint_matrix(A);
	fprintf(stderr,"\nU S V^T = ");
	fprint_matrix(USVT);
	// free memory
	gsl_matrix_free(ATA);
	gsl_matrix_free(D);
	gsl_matrix_free(AV);
	gsl_matrix_free(US);
	gsl_matrix_free(USVT);
}

void sv_solve(gsl_matrix* U, gsl_matrix* S, gsl_matrix* V, gsl_vector* b, gsl_vector* c){
	// solves S V^T c = U^T b for c
	int m = c->size;
	// allocate memory
	gsl_vector* cc = gsl_vector_alloc(m);
	// solve
	gsl_blas_dgemv(CblasTrans, 1.0, U, b, 0.0, cc); // U^T b -> cc
	back_substitution(S, cc); // cc -> y = V^T cc
	gsl_blas_dgemv(CblasNoTrans, 1.0, V, cc, 0.0, c); // c = V y = V V^T cc
	// free memory
	gsl_vector_free(cc);

}

void sv_least_squares_fit(int m, double fit_functions(int j, double x), 
	gsl_vector* x, gsl_vector* y, gsl_vector* dy,
	gsl_vector* c){
	int n = x->size;
	// allocate memory
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* U = gsl_matrix_alloc(n,m); // orthogonal matrix
	gsl_matrix* S = gsl_matrix_alloc(m,m); // matrix of singular values (diagonal)
	gsl_matrix* V = gsl_matrix_alloc(m,m); // orthogonal matrix
	gsl_vector* b = gsl_vector_alloc(n);
	// building A and b 
	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
		for(int j = 0; j < m; j++){
			gsl_matrix_set(A,i,j, fit_functions(j,gsl_vector_get(x,i)) / gsl_vector_get(dy,i) );
		}
	}
	// single value decomposition
	sv_decomposition(A, U, S, V); // builds U, S, and V from A
	// coefficients
	sv_solve(U, S, V, b, c); // solves S V^T c = U^T b for c
	// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(U);
	gsl_matrix_free(S);
	gsl_matrix_free(V);
	gsl_vector_free(b);
}
