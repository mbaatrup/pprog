CFLAGS += -Wall -std=gnu1x $$(gsl-config --cflags)
LDLIBS += $$(gsl-config --libs)
PLOTTER = gnuplot

.PHONY: all clean

all: out_qr.txt out_sv.txt plot_qr.svg plot_sv.svg

out_qr.txt out_sv.txt: main
	./$< > out_qr.txt 2> out_sv.txt

plot_qr.svg: out_qr.txt main Makefile
	echo '\
	set term svg size 800,600 font ",18" background rgb "white";\
	set out "$@";\
	set grid;\
	set title "Least squares fit via QR decomposition";\
	set xlabel "x";\
	set ylabel "y";\
	set tics out scale 0.5;\
	set key out below font ",16";\
	plot\
	"$<" index 0 using 1:2:3 with yerrorbars lc rgb "black" title "Data"\
	,"$<" index 2 using 1:2 with lines lw 3 title "F(x) = c_0 log(x) + c_1 + c_2x"\
	,"$<" index 3 using 1:2 with lines title "F(x) + dc_0 log(x)"\
	,"$<" index 3 using 1:3 with lines title "F(x) - dc_0 log(x)"\
	,"$<" index 4 using 1:2 with lines title "F(x) + dc_1"\
	,"$<" index 4 using 1:3 with lines title "F(x) - dc_1"\
	,"$<" index 5 using 1:2 with lines title "F(x) + dc_2 x"\
	,"$<" index 5 using 1:3 with lines title "F(x) - dc_2 x"\
	'|$(PLOTTER)

plot_sv.svg: out_sv.txt main Makefile
	echo '\
	set term svg size 800,600 font ",18" background rgb "white";\
	set out "$@";\
	set grid;\
	set title "Least squares fit via single value decomposition";\
	set xlabel "x";\
	set ylabel "y";\
	set tics out scale 0.5;\
	set key out below font ",16";\
	plot\
	"$<" index 0 using 1:2:3 with yerrorbars lc rgb "black" title "Data"\
	,"$<" index 2 using 1:2 with lines lw 3 title "F(x) = c_0 log(x) + c_1 + c_2x"\
	'|$(PLOTTER)

main: main.o header.h print.o qr.o sv.o matrix_diagonalization.o

clean:
	$(RM) main *.txt *.o *.svg