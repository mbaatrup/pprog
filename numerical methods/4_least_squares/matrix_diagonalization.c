#include"header.h"

// Jacobi diagonalization, cyclic method
int matrix_diagonalization(gsl_matrix* A, gsl_matrix* V, gsl_matrix* D){
	// A: matrix to be diagonalized
	// V: matrix of eigenvectors (columns) - starts out as identity matrix and is changed through Jacobi rotations
	// D: diagonal matrixwith eigenvalues on the diagonal
	gsl_matrix_set_identity(V);
	int n = A->size1;
	for(int i = 0; i < n; i++){
		gsl_matrix_set(D,i,i,gsl_matrix_get(A,i,i));
	}
	// sweep: do jacobian rotation with all possible p,q values
	int status = 0, sweeps = 0, sweeps_max = 1000;
	do{
		sweeps++;
		for(int p = 0; p < n; p++){
			for(int q = p + 1; q < n; q++){ // q > p
				status = jacobi_rotation(A,V,D,p,q);
			}
		}
	} while(status != 0 && sweeps < sweeps_max); // do again, if status != 0, that is, it hasn't converged
	return sweeps; 
}
	
int jacobi_rotation(gsl_matrix* A, gsl_matrix* V, gsl_matrix* D, int p, int q){
	int status = 0;
	// Jacobi rotation of matrix A, elements ij = pp,qq,pq
	double A_pp = gsl_matrix_get(D,p,p); 
	double A_qq = gsl_matrix_get(D,q,q); 
	double A_pq = gsl_matrix_get(A,p,q);
	double phi = -0.5 * atan2(2 * A_pq, A_pp - A_qq);
	double s = sin(phi);
	double c = cos(phi);
	double A_pp_rot = c*c*A_pp - 2*s*c*A_pq + s*s*A_qq;
	double A_qq_rot = s*s*A_pp + 2*s*c*A_pq + c*c*A_qq;
	double A_pq_rot = 0.0; // s*c*(A_pp - A_qq) + (c*c - s*s)*A_pq; // theoretically zero
	if(A_pp_rot != A_pp || A_qq_rot != A_qq){ 
	// if the d have changed, do rotation - otherwise jump past if-statement.
		status = 1;
		gsl_matrix_set(A,p,q,A_pq_rot);
		gsl_matrix_set(D,p,p,A_pp_rot);
		gsl_matrix_set(D,q,q,A_qq_rot);
	// Jacobi rotation of matrix A, all other elements, ij != pp,qq,pq
		for(int i = 0; i < p; i++){ 
			double A_ip = gsl_matrix_get(A,i,p);
			double A_iq = gsl_matrix_get(A,i,q);
			double A_ip_rot = c*A_ip - s*A_iq;
			double A_iq_rot = s*A_ip + c*A_iq;
			gsl_matrix_set(A,i,p,A_ip_rot);
			gsl_matrix_set(A,i,q,A_iq_rot);
		}
		for(int i = p + 1; i < q; i++){ 
			double A_pi = gsl_matrix_get(A,p,i);
			double A_iq = gsl_matrix_get(A,i,q);
			double A_pi_rot = c*A_pi - s*A_iq;
			double A_iq_rot = s*A_pi + c*A_iq;
			gsl_matrix_set(A,p,i,A_pi_rot);
			gsl_matrix_set(A,i,q,A_iq_rot);
		}
		int n = A->size1;
		for(int i = q + 1; i < n; i++){ 
			double A_pi = gsl_matrix_get(A,p,i);
			double A_qi = gsl_matrix_get(A,q,i);
			double A_pi_rot = c*A_pi - s*A_qi;
			double A_qi_rot = s*A_pi + c*A_qi;
			gsl_matrix_set(A,p,i,A_pi_rot);
			gsl_matrix_set(A,q,i,A_qi_rot);
		}
	// Jacobi rotation of matrix V, elements in columns p and q
		for(int i = 0; i < n; i++){ 
			double V_ip = gsl_matrix_get(V,i,p);
			double V_iq = gsl_matrix_get(V,i,q);
			double V_ip_rot = c*V_ip - s*V_iq;
			double V_iq_rot = s*V_ip + c*V_iq;
			gsl_matrix_set(V,i,p,V_ip_rot);
			gsl_matrix_set(V,i,q,V_iq_rot);
		}
	} 
	return status; // 0: converged, 1: not converged.
}