#include"header.h"

double adapt(double f(double x), double a, double b, double acc, double eps, double f2 , double f3 , double *err, int recursions){
	// this function returns an estimate of the integral
	assert(recursions < 1e6); // the number of recursions is counted because we want to set a limit
	double x1 = a + (b - a)/6, x4 = a + 5*(b - a)/6;
	double f1 = f(x1), f4 = f(x4);
	double Q = (2*f1 + f2 + f3 + 2*f4)*(b - a)/6; // higher order estimate of the integral
	double q = (f1 + f2 + f3 + f4)*(b - a)/4; // lower order estimate of the integral
	double tolerance = acc + eps*fabs(Q);
	*err = fabs(Q - q);
	double err1, err2;
	if(*err < tolerance){
		return Q; // success!
	}
	else{ // use half-intervals
		// smaller interval -> acc is rescaled
		double Q1 = adapt(f, a, (a + b)/2, acc/sqrt(2.0), eps, f1, f2, &err1, recursions + 1);
		double Q2 = adapt(f, (a + b)/2, b, acc/sqrt(2.0), eps, f3, f4, &err2, recursions + 1);
		// total error 
		*err = sqrt(err1*err1 + err2*err2);
		return Q1 + Q2;
	}
}

double adaptive_quadrature(double f(double x), double a, double b, double acc, double eps, double *err){
	// adaptive quadrature: an algorithm where the integration interval is subdivided into 
	// adaptively refined subintervals until a given accuracy goal is reached
	double x2 = a + 2*(b - a)/6, x3 = a + 4*(b - a)/6;
	double f2 = f(x2), f3 = f(x3);
	int recursions = 0;
	double Q = adapt(f, a, b, acc, eps, f2, f3, err, recursions);
	return Q;
}

double adaptive_quadrature_infinite_interval(double f(double x), double a, double b, double acc, double eps, double *err){
	if(isinf(-a) == 1 && isinf(b) == 1){
		a = -1; b = 1;
		double ff(double x){return f(x/(1 - x*x))* (1 + x*x)/pow(1 - x*x,2);}
		double x2 = a + 2*(b - a)/6, x3 = a + 4*(b - a)/6;
		double f2 = f(x2), f3 = f(x3);
		int recursions = 0;
		double Q = adapt(ff, a, b, acc, eps, f2, f3, err, recursions);
		return Q;
	}
	else if(isinf(b) == 1){
		a = 0; b = 1;
		double ff(double x){return f(a + x/(1 - x)) * 1/pow(1 - x, 2);}
		double x2 = a + 2*(b - a)/6, x3 = a + 4*(b - a)/6;
		double f2 = f(x2), f3 = f(x3);
		int recursions = 0;
		double Q = adapt(ff, a, b, acc, eps, f2, f3, err, recursions);
		return Q;
	}
	else if(isinf(-a) == 1){
		a = 0; b = 1;
		double ff(double x){return f(a - (1 - x)/x) * 1/(x*x);}
		double x2 = a + 2*(b - a)/6, x3 = a + 4*(b - a)/6;
		double f2 = f(x2), f3 = f(x3);
		int recursions = 0;
		double Q = adapt(ff, a, b, acc, eps, f2, f3, err, recursions);
		return Q;
	}
	else{
		double x2 = a + 2*(b - a)/6, x3 = a + 4*(b - a)/6;
		double f2 = f(x2), f3 = f(x3);
		int recursions = 0;
		double Q = adapt(f, a, b, acc, eps, f2, f3, err, recursions);
		return Q;
	}
}
