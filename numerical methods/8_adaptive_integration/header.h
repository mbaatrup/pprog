#ifndef HEADER 
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<assert.h>

double adapt(double f(double), double a, double b, double acc, double eps, double f2 , double f3 , double *err, int recursions);
double adaptive_quadrature(double f(double), double a, double b, double acc, double eps, double *err);
double adaptive_quadrature_infinite_interval(double f(double x), double a, double b, double acc, double eps, double *err);


#define HEADER 
#endif