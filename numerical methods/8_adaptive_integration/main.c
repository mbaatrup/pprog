#include"header.h"
#define FMT "%.7e"

int main(){ 
	printf("Adaptive integration\n");
	double a = 0, b = 1; // lower and upper limit of integration
	double err; // estimated errer
	double acc = 1e-6, eps = 1e-6; // absolute and relative accuracy goals
	int calls; // function calls

	// integrands
	double integrand_1(double x){calls++; return sqrt(x);} // exact: int( sqrt(x), 0, 1) = 2/3
	double integrand_2(double x){calls++; return 1.0/sqrt(x);} // exact: int( 1/sqrt(x), 0, 1) = 2
	double integrand_3(double x){calls++; return log(x)/sqrt(x);} // exact: int( ln(x)/sqrt(x), 0, 1) = -4
	double integrand_4(double x){calls++; return 4*sqrt( 1 - (1 - x)*(1 - x) );} // exact: int( 4*sqrt(1 - (1 - x)²), 0, 1) = pi
	double integrand_4_gsl(double x, void* params){calls++; return 4*sqrt( 1 - (1 - x)*(1 - x));}
	double integrand_5(double x){calls++; return exp(-x);} // exact: int(exp(-x), 0, infty) = 1
	double integrand_6(double x){calls++; return exp(x);} // exact: int(exp(x), -infty, 0) = 1
	double integrand_7(double x){calls++; return exp(-x*x);} // exact: int(exp(-x^2), -infty, infty) = sqrt(pi)
	double integrand_7_gsl(double x, void* params){calls++; return exp(-x*x);}
	
	printf("\nIntegrand: sqrt(x)\n");
	err = 0; calls = 0;
	double I = 2.0/3.0;
	double Q = adaptive_quadrature(integrand_1, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: 1/sqrt(x)\n");
	err = 0; calls = 0;
	I = 2.0;
	Q = adaptive_quadrature(integrand_2, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: ln(x)/sqrt(x)\n");
	err = 0; calls = 0;
	I = -4.0;
	Q = adaptive_quadrature(integrand_3, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: 4*sqrt(1 - (1 - x)²)\n");
	err = 0; calls = 0;
	I = M_PI;
	Q = adaptive_quadrature(integrand_4, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: exp(-x)\n");
	a = 0; b = INFINITY; err = 0; calls = 0;
	I =  1e0;
	Q = adaptive_quadrature_infinite_interval(integrand_5, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: exp(x)n");
	a = -INFINITY; b = 0; err = 0; calls = 0;
	I =  1e0;
	Q = adaptive_quadrature_infinite_interval(integrand_6, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: exp(-x²)\n");
	a = -INFINITY; b = INFINITY; err = 0; calls = 0;
	I = sqrt(M_PI);
	Q = adaptive_quadrature_infinite_interval(integrand_7, a, b, acc, eps, &err);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

/////////////////////////////////////////////////////////////////////////////////

	printf("\nIntegration using the gsl_integration_qagi routine\n");
	size_t limit = 1e3;
	gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(limit);	
	gsl_function F; 
	double Q_gsl, err_gsl;

	printf("\nIntegrand: 4*sqrt(1 - (1 - x)²)\n");
	a = 0; b = 1; err = 0; calls = 0;
	double I_gsl = M_PI;
	F.function = &integrand_4_gsl;
	gsl_integration_qags(&F, a, b, acc, eps, limit, workspace, &Q_gsl, &err_gsl);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I_gsl);
	printf("Estimated result: Q = "FMT"\n", Q_gsl);
	printf("Estimated error: err = "FMT"\n", err_gsl);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I_gsl - Q_gsl));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	printf("\nIntegrand: exp(-x²)\n");
	a = -INFINITY; b = INFINITY; err = 0; calls = 0;
	I_gsl = sqrt(M_PI);
	F.function = &integrand_7_gsl;
	gsl_integration_qagi(&F, acc, eps, limit, workspace, &Q_gsl, &err_gsl);
	printf("Integration interval: [%g,%g]\n", a, b);
	printf("Exact result: I = "FMT"\n", I_gsl);
	printf("Estimated result: Q = "FMT"\n", Q_gsl);
	printf("Estimated error: err = "FMT"\n", err_gsl);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I_gsl - Q_gsl));
	printf("Accuracy goals: acc = %g, eps = %g\n", acc, eps);
	printf("Number of function calls: %i\n", calls);

	gsl_integration_workspace_free(workspace);

	return 0;
}
