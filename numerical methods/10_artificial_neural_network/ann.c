#include"header.h"

ann* ann_alloc(int n, double(*f)(double)){ 
	// allocating memory to the structure and filling it
	ann* nw = malloc(sizeof(ann)); // nw: network
	nw->n = n; // number of hidden neurons
	nw->f = f; // activation function
	nw->params = gsl_vector_alloc(3*n); // three parameters per neuron
	return nw;
}

ann* ann_alloc_2(int n, double(*f)(double)){ 
	// allocating memory to the structure and filling it
	ann* nw = malloc(sizeof(ann)); // nw: network
	nw->n = n; // number of hidden neurons
	nw->f = f; // activation function
	nw->params = gsl_vector_alloc(5*n); // five parameters per neuron
	return nw;
}

void ann_free(ann* nw){ // nw is changed
	gsl_vector_free(nw->params);
	free(nw);
}

double ann_feed_forward(ann* nw, double xj){ // nw and xj are not changed
	// from input (x) to output (Fx)
	int n = nw->n; // number of hidden neurons
	double Fj = 0; // output from network
	for(int i = 0; i < n; i++){
		double ai = gsl_vector_get(nw->params, 3*i);
		double bi = gsl_vector_get(nw->params, 3*i + 1);
		double wi = gsl_vector_get(nw->params, 3*i + 2);
		Fj += nw->f((xj + ai)/bi)*wi;
	}
	return Fj;
}

double ann_feed_forward_2(ann* nw, double xj, double yj){ // nw and xj are not changed
	// from input (x) to output (Fx)
	int n = nw->n; // number of hidden neurons
	double Fj = 0; // output from network
	for(int i = 0; i < n; i++){
		double ai = gsl_vector_get(nw->params, 5*i);
		double bi = gsl_vector_get(nw->params, 5*i + 1);
		double ci = gsl_vector_get(nw->params, 5*i + 2);
		double di = gsl_vector_get(nw->params, 5*i + 3);
		double wi = gsl_vector_get(nw->params, 5*i + 4);
		Fj += nw->f((xj + ai)/bi)*nw->f((yj + ci)/di)*wi;
	}
	return Fj;
}

void ann_train(ann* nw, gsl_vector* x, gsl_vector* y){ // nw (params) is changed, x and y are not
	int N = x->size; // number of data points
	int n = nw->n; // number of hidden neurons
	fprintf(stderr,"Number of data points: %i\n", N);
	fprintf(stderr,"Number of hidden neurons: %i\n", n);
	// deviation - nested function
	double deviation(const gsl_vector* p, void* nothing){// p: vector of parameters
		gsl_vector_memcpy(nw->params, p);
		double F = 0;
		for(int j = 0; j < N; j++){
			double xj = gsl_vector_get(x,j);
			double yj = gsl_vector_get(y,j);
			double Fj = ann_feed_forward(nw,xj);
			F += pow(Fj - yj,2);
		}
		return F;
	}
	// minimization of the deviation
	double acc = 1e-3;
	int iter = minimization(deviation, nw->params, n, acc); // params is changed
	double dev = deviation(nw->params, NULL);
	fprintf(stderr,"Accuracy goal: %.0e\n", acc);
	fprintf(stderr,"Deviation: %.3e\n", dev);
	fprintf(stderr,"Iterations: %i\n", iter);
}

void ann_train_2(ann* nw, gsl_vector* x, gsl_vector* y, gsl_matrix* g){ // nw (params) is changed; x, y, and g are not
	int N = x->size; // number of data points
	int n = nw->n; // number of hidden neurons
	fprintf(stderr,"Number of data points: %i\n", N);
	fprintf(stderr,"Number of hidden neurons: %i\n", n);
	// deviation - nested function
	double deviation(const gsl_vector* p, void* nothing){// p: vector of parameters
		gsl_vector_memcpy(nw->params, p);
		double F = 0;
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				double xi = gsl_vector_get(x,i);
				double yj = gsl_vector_get(y,j);
				double gij = gsl_matrix_get(g,i,j);
				double Fij = ann_feed_forward_2(nw, xi, yj);
				F += pow(Fij - gij, 2);
			}
		}
		return F;
	}
	// minimization of the deviation
	double acc = 1e-3;
	int iter = minimization_2(deviation, nw->params, n, acc); // params is changed
	double dev = deviation(nw->params, NULL);
	fprintf(stderr,"Accuracy goal: %.0e\n", acc);
	fprintf(stderr,"Deviation: %.3e\n", dev);
	fprintf(stderr,"Iterations: %i\n", iter);

}
