#include"header.h"
#define FMT "%.3e"

double activation_function(double x){return x*exp(-x*x);}
double function_to_fit(double x){return cos(5*x - 1)*exp(-x*x);}
double function_to_fit_2(double x, double y){return exp(-x*x - y*y);}

int main(){ 
	int n = 5; // number of hidden neurons 
	int N = 20; // number of data points
	double a = -1, b = 1; // lower and upper limits

	// allocate memory
	ann* nw = ann_alloc(n, activation_function);
	ann* nw_2 = ann_alloc_2(n, activation_function);
	gsl_vector* x = gsl_vector_alloc(N);
	gsl_vector* y = gsl_vector_alloc(N);
	gsl_matrix* g = gsl_matrix_alloc(N,N);

	fprintf(stderr,"ANN interpolation of 1D tabulated function: cos(5x - 1) exp(-x²)\n");
	// data
	printf("x \ty \n");
	for(int j = 0; j < N; j++){
		double xj = a + (b - a) *j/(N - 1);
		double yj = function_to_fit(xj);
		printf(""FMT" \t"FMT"\n", xj, yj);
		gsl_vector_set(x,j, xj);
		gsl_vector_set(y,j, yj);
	}
	printf("\n\n");
	// initial parameter values
	for(int i = 0; i < n; i++){
		gsl_vector_set(nw->params, 3*i, a + (b - a) *i/(n - 1)); // a
		gsl_vector_set(nw->params, 3*i + 1, 1); // b
		gsl_vector_set(nw->params, 3*i + 2, 1); // w
	}
	// train network
	ann_train(nw, x, y);
	// fit
	printf("x \tF \t|F - y| \n");
	double step = 1e-2;
	for(double xx = a; xx < b; xx += step){ 
		double yy = function_to_fit(xx);
		double F = ann_feed_forward(nw, xx);
		double dev = fabs(F - yy); // deviation
		printf(""FMT" \t"FMT" \t"FMT" \n", xx, F, dev);
	}

	fprintf(stderr,"\nANN interpolation of 2D tabulated function: exp(-x² - y²)\n");
	// data
	printf("\n\n");
	printf("x \ty \tg\n");
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			double xi = a + (b - a) *i/(N - 1);
			double yj = a + (b - a) *j/(N - 1);
			double gij = function_to_fit_2(xi, yj);
			printf(""FMT" \t"FMT" \t"FMT"\n", xi, yj, gij);
			gsl_vector_set(x,i, xi);
			gsl_vector_set(y,j, yj);
			gsl_matrix_set(g,i,j, gij);
		}
	}
	printf("\n\n");
	// initial parameter values
	for(int i = 0; i < n; i++){
		gsl_vector_set(nw_2->params, 5*i, a + (b - a) *i/(n - 1)); // a
		gsl_vector_set(nw_2->params, 5*i + 1, 1); // b
		gsl_vector_set(nw_2->params, 5*i + 2, a + (b - a) *i/(n - 1)); // c
		gsl_vector_set(nw_2->params, 5*i + 3, 1); // d
		gsl_vector_set(nw_2->params, 5*i + 4, 1); // w
	}
	// train network
	ann_train_2(nw_2, x, y, g);
	// fit
	printf("x \ty \tF \t|F - g| \n");
	double step_2 = 1e-1;
	for(double xx = a; xx < b; xx += step_2){
		for(double yy = a; yy < b; yy += step_2){
			double gg = function_to_fit_2(xx, yy);
			double F = ann_feed_forward_2(nw_2, xx, yy);
			double dev = fabs(F - gg); // deviation
			printf(""FMT" \t"FMT" \t"FMT" \t"FMT" \n", xx, yy, F, dev);
		}
	}

	// free memory
	ann_free(nw);
	ann_free(nw_2);
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_matrix_free(g);
	
	return 0;
}
