#include"header.h"
#include<gsl/gsl_multimin.h> 
#define ITER_MAX 1e6

int minimization(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, int n, double acc){
	int nn = 3*n; // total number of parameters to be optimized: 3*n
	// multimin function
	gsl_multimin_function multimin_function;
	multimin_function.f = deviation;
	multimin_function.n = nn;
	multimin_function.params = NULL;
	// starting point
	gsl_vector *initial_guess = gsl_vector_alloc(nn);
	gsl_vector_memcpy(initial_guess,p);
	// stepsize
	double initial_stepsize = 1;
	gsl_vector *stepsize = gsl_vector_alloc(nn);
	gsl_vector_set_all(stepsize, initial_stepsize);
	// initialization
	const gsl_multimin_fminimizer_type *type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *solver = gsl_multimin_fminimizer_alloc(type, nn);
	gsl_multimin_fminimizer_set(solver, &multimin_function, initial_guess, stepsize);
	// iterations	
	int status, iter = 0; 
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(solver);
		if(status)break;

		status = gsl_multimin_test_size(solver->size, acc);
		if(status == GSL_SUCCESS){
			fprintf(stderr,"Minimization status: %s\n", gsl_strerror(status));
		}
	}
	while(status == GSL_CONTINUE && iter < ITER_MAX);
	// result
	for(int i = 0; i < n; i++){
		gsl_vector_set(p, 3*i, gsl_vector_get(solver->x, 3*i)); // a
		gsl_vector_set(p, 3*i + 1, gsl_vector_get(solver->x, 3*i + 1)); // b
		gsl_vector_set(p, 3*i + 2, gsl_vector_get(solver->x, 3*i + 2)); // w
	}
	// free memory
	gsl_vector_free(initial_guess);
	gsl_vector_free(stepsize);
	gsl_multimin_fminimizer_free(solver);

	return iter;
}

int minimization_2(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, int n, double acc){
	int nn = 5*n; // total number of parameters to be optimized: 3*n
	// multimin function
	gsl_multimin_function multimin_function;
	multimin_function.f = deviation;
	multimin_function.n = nn;
	multimin_function.params = NULL;
	// starting point
	gsl_vector *initial_guess = gsl_vector_alloc(nn);
	gsl_vector_memcpy(initial_guess,p);
	// stepsize
	double initial_stepsize = 1;
	gsl_vector *stepsize = gsl_vector_alloc(nn);
	gsl_vector_set_all(stepsize, initial_stepsize);
	// initialization
	const gsl_multimin_fminimizer_type *type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *solver = gsl_multimin_fminimizer_alloc(type, nn);
	gsl_multimin_fminimizer_set(solver, &multimin_function, initial_guess, stepsize);
	// iterations	
	int status, iter = 0; 
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(solver);
		if(status)break;

		status = gsl_multimin_test_size(solver->size, acc);
		if(status == GSL_SUCCESS){
			fprintf(stderr,"Minimization status: %s\n", gsl_strerror(status));
		}
	}
	while(status == GSL_CONTINUE && iter < ITER_MAX);
	// result
	for(int i = 0; i < n; i++){
		gsl_vector_set(p, 5*i, gsl_vector_get(solver->x, 5*i)); // a
		gsl_vector_set(p, 5*i + 1, gsl_vector_get(solver->x, 5*i + 1)); // b
		gsl_vector_set(p, 5*i + 2, gsl_vector_get(solver->x, 5*i + 2)); // c
		gsl_vector_set(p, 5*i + 3, gsl_vector_get(solver->x, 5*i + 3)); // d
		gsl_vector_set(p, 5*i + 4, gsl_vector_get(solver->x, 5*i + 4)); // w
	}
	// free memory
	gsl_vector_free(initial_guess);
	gsl_vector_free(stepsize);
	gsl_multimin_fminimizer_free(solver);

	return iter;
}