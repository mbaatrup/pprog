#ifndef HEADER
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

// ann: artificial neural network
// nw: network
// n: number of hidden neurons
// f: activation function
// x, y, g: tabulated function
typedef struct {int n; double (*f)(double); gsl_vector* params;} ann; 
ann* ann_alloc(int n, double (*f)(double)); 
ann* ann_alloc_2(int n, double(*f)(double));
void ann_free(ann* nw); 
double ann_feed_forward(ann* nw, double xj);
double ann_feed_forward_2(ann* nw, double xj, double yj);
void ann_train(ann* nw, gsl_vector* x, gsl_vector* y);
void ann_train_2(ann* nw, gsl_vector* x, gsl_vector* y, gsl_matrix* g);

// minimization
int minimization(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, int n, double acc);
int minimization_2(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, int n, double acc);

#define HEADER
#endif
