#include"header.h"

int main(int argc, char* argv[]){
	int n = 3; // default
	if(argc > 1){
		n = atoi(argv[1]);
	}

	printf("Task A: Jacobi diagonalization with cyclic sweeps\n");
	task_A(n);

	printf("\nTask B: Jacobi diagonalization eigenvalue-by-eigenvalue\n\n");
	task_B(n);

	printf("\nTask AB: Comparing number of sweeps (rotations)\n\n");
	task_AB(n);

	printf("\nTask C: Classic Jacobi eigenvalue algorithm\n\n");
	task_C(n);

	printf("\nTime results for Jacobi diagonalization of nxn matrix:\n");
	printf("\tn   time (sec)\n");
	FILE* time_A = fopen("time_A.txt","r"); // r: reading
	char save_A[10];
	while(!feof(time_A)){
		fgets(save_A, sizeof(save_A), time_A);
	}
	printf("Cyclic: %s\n", save_A);
	fclose(time_A);
	FILE* time_C = fopen("time_C.txt","r");
	char save_C[10];
	while(!feof(time_C)){
		fgets(save_C, sizeof(save_C), time_C);
	}
	printf("Classic: %s\n", save_C);
	fclose(time_C);

	return 0;
}