#include"header.h"

int jacobi_eigenvalue_by_eigenvalue(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int row, int sort){
	// sort == 1 - eigenvalues are sorted in descending order
	// sort != 1 - eigenvalues are sorted ascending order
	// V: matrix of eigenvectors (columns) - starts out as identity matrix and is changed through Jacobi rotations
	// d: vector of diagonal values - becomes eigenvalues af Jacobi rotation sweeps
	gsl_matrix_set_identity(V); 
	int n = A->size1;
	for(int i = 0; i < n; i++){
		gsl_vector_set(d,i,gsl_matrix_get(A,i,i)); 
	}
	// sweep: do jacobian rotation for a limited number of rows
	int status = 0, sweeps = 0, sweeps_max = 1000;
	for(int p = 0; p < n && p < row; p++){
	do{ 
		sweeps++;
			for(int q = p + 1; q < n; q++){ // q > p
				status = jacobi_rotation(A,V,d,p,q,sort);
			}
		}while(status != 0 && sweeps < sweeps_max); // do again, if status != 0, that is, it hasn't converged
	} 
	return sweeps; 
}
