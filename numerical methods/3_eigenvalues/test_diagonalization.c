#include"header.h"

void test_diagonalization(gsl_matrix* A, gsl_matrix* V, gsl_vector* d){
	int n = A->size1;

	// allocate memory
	gsl_matrix* D = gsl_matrix_calloc(n,n); 
	gsl_matrix* AV = gsl_matrix_calloc(n,n);
	gsl_matrix* VTAV = gsl_matrix_calloc(n,n);

	// test
	for(int i = 0; i < n; i++){
		gsl_matrix_set(D,i,i,gsl_vector_get(d,i));
	}
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, V, 0, AV);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1.0, V, AV, 0, VTAV);
	printf("V^TAV =");
	print_matrix(VTAV);
	printf("D =");
	print_matrix(D);

	// free memory
	gsl_matrix_free(D);
	gsl_matrix_free(AV);
	gsl_matrix_free(VTAV);
}
