#include"header.h"

void task_AB(int n){
	// allocate memory
	gsl_matrix* A = gsl_matrix_calloc(n,n);
	gsl_matrix* V = gsl_matrix_calloc(n,n);
	gsl_vector* d = gsl_vector_calloc(n);

	// symmetric random matrix
	symmetric_matrix(A);

	// diagonalize using cyclic and lowest eigenvalue. compare sweeps
	printf("Matrix dimensions: nxn\n");
	printf("Method \t\tn \tSweeps");
	int i = 0;
	do{
		if(i == 0){
			printf("\n[Ascending eigenvalues]\n");
		}else{
			printf("\n[Descending eigenvalues]\n");
		}
		int sweeps_full = jacobi_cyclic(A, V, d, i);
		matrix_rebuild_sym(A);
		int sweeps_row = jacobi_eigenvalue_by_eigenvalue(A, V, d, 1, i);
		matrix_rebuild_sym(A);
		int sweeps_all_rows = jacobi_eigenvalue_by_eigenvalue(A, V, d, n, i);
		matrix_rebuild_sym(A);
		printf("Cyclic \t\t%i \t%i \n", n, sweeps_full);
		printf("Value-by-value \t1 \t%i \n", sweeps_row);
		printf("Value-by-value \t%i \t%i \n", n, sweeps_all_rows);
		i++;
	}while(i <= 1);

	// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(d);
}