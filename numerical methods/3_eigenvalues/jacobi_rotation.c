#include"header.h"

int jacobi_rotation(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int p, int q, int sort){
	int status = 0;
	// Jacobi rotation of matrix A, elements ij = pp,qq,pq
	double A_pp = gsl_vector_get(d,p); 
	double A_qq = gsl_vector_get(d,q); 
	double A_pq = gsl_matrix_get(A,p,q);
	double phi;
	if (sort == 1){
		// sort == 1 - eigenvalues are sorted in descending order
		// sort != 1 - eigenvalues are sorted ascending order
		phi = -0.5 * atan2(2 * A_pq, A_pp - A_qq);
	}else{
		phi = 0.5 * atan2(2 * A_pq, A_qq - A_pp);
	}
	double s = sin(phi);
	double c = cos(phi);
	double A_pp_rot = c*c*A_pp - 2*s*c*A_pq + s*s*A_qq;
	double A_qq_rot = s*s*A_pp + 2*s*c*A_pq + c*c*A_qq;
	double A_pq_rot = 0.0; // s*c*(A_pp - A_qq) + (c*c - s*s)*A_pq; // theoretically zero
	if(A_pp_rot != A_pp || A_qq_rot != A_qq){ 
	// if the d have changed, do rotation - otherwise jump past if-statement.
		status = 1;
		gsl_matrix_set(A,p,q,A_pq_rot);
		gsl_vector_set(d,p,A_pp_rot);
		gsl_vector_set(d,q,A_qq_rot);
	// Jacobi rotation of matrix A, all other elements, ij != pp,qq,pq
		for(int i = 0; i < p; i++){ 
			double A_ip = gsl_matrix_get(A,i,p);
			double A_iq = gsl_matrix_get(A,i,q);
			double A_ip_rot = c*A_ip - s*A_iq;
			double A_iq_rot = s*A_ip + c*A_iq;
			gsl_matrix_set(A,i,p,A_ip_rot);
			gsl_matrix_set(A,i,q,A_iq_rot);
		}
		for(int i = p + 1; i < q; i++){ 
			double A_pi = gsl_matrix_get(A,p,i);
			double A_iq = gsl_matrix_get(A,i,q);
			double A_pi_rot = c*A_pi - s*A_iq;
			double A_iq_rot = s*A_pi + c*A_iq;
			gsl_matrix_set(A,p,i,A_pi_rot);
			gsl_matrix_set(A,i,q,A_iq_rot);
		}
		int n = A->size1;
		for(int i = q + 1; i < n; i++){ 
			double A_pi = gsl_matrix_get(A,p,i);
			double A_qi = gsl_matrix_get(A,q,i);
			double A_pi_rot = c*A_pi - s*A_qi;
			double A_qi_rot = s*A_pi + c*A_qi;
			gsl_matrix_set(A,p,i,A_pi_rot);
			gsl_matrix_set(A,q,i,A_qi_rot);
		}
	// Jacobi rotation of matrix V, elements in columns p and q
		for(int i = 0; i < n; i++){ 
			double V_ip = gsl_matrix_get(V,i,p);
			double V_iq = gsl_matrix_get(V,i,q);
			double V_ip_rot = c*V_ip - s*V_iq;
			double V_iq_rot = s*V_ip + c*V_iq;
			gsl_matrix_set(V,i,p,V_ip_rot);
			gsl_matrix_set(V,i,q,V_iq_rot);
		}
	} 
	return status; // 0: converged, 1: not converged.
}
