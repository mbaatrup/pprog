#include"header.h"

// determines the largest element in row i of a matrix A
int largest_element(gsl_matrix* A, int i){
	// i: row number
	// i + 1: first off-diagonal element
	// j: column number
	// index: index of the largest element in the row
	// m: number of elements in the row
	int m = A->size1;
	int index = i + 1; // "initial guess" of the index
	for(int j = i + 2; j < m; j++){
		if(pow(gsl_matrix_get(A,i,j),2) > pow(gsl_matrix_get(A,i,index),2)){
			index = j; // update index
		}
	}
	return index; // final index
}

int jacobi_classic(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int sort){
	// sort == 1 - eigenvalues are sorted in descending order
	// sort != 1 - eigenvalues are sorted ascending order
	// V: matrix of eigenvectors (columns) - starts out as identity matrix and is changed through Jacobi rotations
	// d: vector of diagonal values - becomes eigenvalues af Jacobi rotation sweeps
	// indices: vector of indices of largest off-diagonal element in each row
	gsl_matrix_set_identity(V); 
	int n = A->size1;
	gsl_vector* indices = gsl_vector_alloc(n);

	for(int i = 0; i < n; i++){
		gsl_vector_set(d,i, gsl_matrix_get(A,i,i));
		gsl_vector_set(indices,i, largest_element(A,i));	 
	}
	// sweep: do jacobian rotation with all possible p,q values
	int status = 0, sweeps = 0, sweeps_max = 1000, q = 0;
	do{

		for(int p = 0; p < n - 1; p++){ // for each row with off-diagonal elements
			sweeps++;
			q = gsl_vector_get(indices,p); // get index of largest element in row p
			status = jacobi_rotation(A,V,d,p,q,sort); // the rotation affects indices
			gsl_vector_set(indices,p, largest_element(A,p)); // update indices
			gsl_vector_set(indices,q, largest_element(A,q)); // update indices
		}
	} while(status != 0 && sweeps < sweeps_max); // do again, if status != 0, that is, it hasn't converged

	gsl_vector_free(indices);
	return sweeps; 
}
