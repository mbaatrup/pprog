#include"header.h"

void task_C(int n){
	// allocate memory
	gsl_matrix* A = gsl_matrix_calloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_calloc(n,n);
	gsl_matrix* V = gsl_matrix_calloc(n,n); // matrix of eigenvectors - starts out as identity matrix and is changed through Jacobi rotations
	gsl_vector* d = gsl_vector_calloc(n); // vector of diagonal values - becomes eigenvalues after Jacobi rotation s

	// symmetric random  matrix
	symmetric_matrix(A);
	gsl_matrix_memcpy(A_copy, A);
	printf("Symmetric random matrix:\nA = ");
	print_matrix(A);

	// diagonalization	
	int sweeps = jacobi_classic(A, V, d, 0);
	printf("\nDiagonalization:\n\n");
	printf("Number of sweeps: %i\n",sweeps);
	printf("Matrix of eigenvectors:\nV = ");
	print_matrix(V);
	printf("Eigenvalues:");
	print_vector(d);

	test_diagonalization(A_copy, V, d);
	printf("V^TAV = D, as it should be\n\n");

	// free memory
	gsl_matrix_free(A_copy);
	gsl_matrix_free(V);
	gsl_vector_free(d);
}
