#include"header.h"

// reconstructing symmetric matrix A from its (unchanged) lower triangular part
void matrix_rebuild_sym(gsl_matrix* A){
	for (int i = 0; i < A->size1; i++){	// row
		for (int j = 0; j < A->size2; j++){	// column
			gsl_matrix_set(A,i,j,
				gsl_matrix_get(A,j,i));
		}
	}
}
