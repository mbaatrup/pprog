#ifndef HEADER // if the header file is not already included, then include it
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#include<stdlib.h>

// tasks
void task_A(int n);
void task_B(int n);
void task_AB(int n);
void task_C(int n);

void symmetric_matrix(gsl_matrix* M); // generate symmetric matrix (fill with random numbers) 
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);

int jacobi_rotation(gsl_matrix* A, gsl_matrix* V, gsl_vector* eigenvalues, int p, int q, int sort);
int jacobi_cyclic(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int sort);
int jacobi_eigenvalue_by_eigenvalue(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int row, int sort);
int jacobi_classic(gsl_matrix* A, gsl_matrix* V, gsl_vector* d, int sort);

int largest_element(gsl_matrix* A, int i); // determines the largest element in row i of matrix A
void matrix_rebuild_sym(gsl_matrix * A); // reconstructing symmetric matrix A from its (unchanged) lower triangular part
void test_diagonalization(gsl_matrix* A, gsl_matrix* V, gsl_vector*d); // computes V^T A V and prints it together with a matrix D with eigenvalueson the diagonal

#define HEADER // the header file is included and will not be included again 
#endif