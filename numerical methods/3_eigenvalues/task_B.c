#include"header.h"

void task_B(int n){
	// allocate memory
	gsl_matrix* A = gsl_matrix_calloc(n,n);
	gsl_matrix* V = gsl_matrix_calloc(n,n); // matrix of eigenvectors - starts out as identity matrix and is changed through Jacobi rotations
	gsl_vector* d = gsl_vector_calloc(n); // vector of diagonal values

	// symmetric random matrix
	symmetric_matrix(A);
	printf("Symmetric random matrix:\nA = ");
	print_matrix(A);

	// diagonalization eigenvalue-by-eigenvalue
	printf("\nDiagonalization:\n\n");
	for(int number_of_eigenvalues = 1; number_of_eigenvalues < n + 1; number_of_eigenvalues++){
		// allocate memory
		gsl_vector* eigenvalues = gsl_vector_calloc(number_of_eigenvalues);
		// number of eigenvalues to calculate
		printf("Number of eigenvalues: %i\n", number_of_eigenvalues);
		// diagonalization and number of sweeps
		int sweeps = jacobi_eigenvalue_by_eigenvalue(A, V, d, number_of_eigenvalues, 0);
		printf("Number of sweeps: %i\n", sweeps);
		// reconstructing matrix A from its (unchanged) lower triangular part
		matrix_rebuild_sym(A);
		// vector of eigenvalues
		for(int i = 0; i < number_of_eigenvalues; i++){
			gsl_vector_set(eigenvalues,i, gsl_vector_get(d,i));
		}
		printf("Eigenvalues:");
		print_vector(eigenvalues);
		// free memory
		gsl_vector_free(eigenvalues);
	}

	// eigenvalues in descending order (changing "sort" from 0 to 1)
	jacobi_cyclic(A, V, d, 1);
	matrix_rebuild_sym(A);
	printf("\nEigenvalues in descending order:");
	print_vector(d);

	// free memory
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(d);
}
