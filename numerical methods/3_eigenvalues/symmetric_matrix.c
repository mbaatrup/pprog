#include"header.h"
#define RND ((double)rand()/RAND_MAX);

void symmetric_matrix(gsl_matrix* M){
// fills matrix with random numbers symmetrically wrt the diagonal
	int n = M->size1;
	for(int i = 0; i < n; i++){
		for(int j = i; j < n; j++){
			double random_number = RND;
			gsl_matrix_set(M,i,j,random_number);
			gsl_matrix_set(M,j,i,random_number);
		}
	}
}
