#include"header.h"
#define STEPS_MAX 1e6
#define CALLS_MAX 1e6
#define LAMBDA_MIN 1e-3
#define NTIMES_MAX 50

void newton(void f(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double eps){
	int n = x->size, steps = 0, calls = 0;

	// allocate memory
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* xx = gsl_vector_alloc(n);
	gsl_vector* fxx = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);

	f(x,fx);
	calls++;

	do{ // take a step
		// Jacobian matrix
		for(int j = 0; j < n; j++){
			// change xj to xj + dx
			gsl_vector_set(x,j, gsl_vector_get(x,j) + dx);
			// calculate finite difference: df = f(x1,...,xj + dx,...,xn) - f(x1,...,xn)
			f(x,df); // first: df = f(x1,...,xj + dx,...,xn)
			calls++;
			gsl_vector_sub(df,fx); // then: df = f(x1,...,xj + dx,...,xn) - f(x1,...,xn)
			// calculate partial derivatives and store in Jacobian matrix
			for(int i = 0; i < n; i++){
				gsl_matrix_set(J,i,j, gsl_vector_get(df,i) / dx);
			}
			// change xj + dx back to xj
			gsl_vector_set(x,j, gsl_vector_get(x,j) - dx);
		}
		// Newton's step: solve J*Dx = -fx for Dx through QR decomposition of J
		qr_decomposition(J,R); // remember, J is changed here
		qr_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1); // Dx result
		// backtracking line search
		double lambda = 1;
		do{
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down
			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			f(xx,fxx); // fill fxx with function values
			calls++;
 			// stop criteria:
			if(gsl_blas_dnrm2(fxx) < (1 - lambda/2)*gsl_blas_dnrm2(fx) || lambda < LAMBDA_MIN) break;	
			// stop if ||f(x + lambda*Dx)|| < (1 - lambda/2) ||f(x)|| OR if lambda is too small
			// otherwise continue: modify lambda and thereby step size
			lambda /= 2.0; // lambda <- lambda/2
			gsl_vector_scale(Dx,0.5); // Dx <- Dx/2
		}while(calls < CALLS_MAX);
		// save result - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(fx,fxx); // fx <- fxx
	steps++;
	 }while(gsl_blas_dnrm2(fx) > eps && gsl_blas_dnrm2(Dx) > dx && steps < STEPS_MAX); 
	// continue if not converged and stepsize not too small - otherwise stop
	// no more steps

	printf("Steps: %i\n",steps);
	printf("Function calls: %i\n",calls);
	printf("Function values (should be close to zero):\n");
	for(int i = 0; i < n; i++){
		int function_number = i + 1;
		printf("\tf%i(x,y) = "FMT"\n", function_number, gsl_vector_get(fx,i));
	}

	// free memory
	gsl_vector_free(fx);
	gsl_vector_free(df);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(Dx);
	gsl_vector_free(xx);
	gsl_vector_free(fxx);
}

void newton_with_jacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double eps){
	int n = x->size, steps = 0, calls = 0;

	// allocate memory
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_vector* xx = gsl_vector_alloc(n);
	gsl_vector* fxx = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	
	f(x,fx,J); // void eq_system_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);
	calls++;

	do{ // take a step
		// Newton's step: solve J*Dx = -fx for Dx through QR decomposition of J
		gsl_matrix_memcpy(Q,J);
		qr_decomposition(Q,R); 
		qr_solve(Q,R,fx,Dx);
		gsl_vector_scale(Dx,-1); // Dx result
		// backtracking line search
		double lambda = 1;
		do{
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down
			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			f(xx,fxx,J); // fill fxx with function values
			calls++;
			if(gsl_blas_dnrm2(fxx) < (1 - lambda/2)*gsl_blas_dnrm2(fx) || lambda < LAMBDA_MIN) break;	
			// stop if: ||f(x + lambda*Dx)|| < (1 - lambda/2) ||f(x)|| OR if lambda is too small
			// otherwise continue: modify lambda and thereby step size
			lambda /= 2.0; // lambda <- lambda/2
			gsl_vector_scale(Dx,0.5); // Dx <- lambda*Dx
		}while(calls < CALLS_MAX);
		// save result - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(fx,fxx); // fx <- fxx
		steps++;
	} while(gsl_blas_dnrm2(fx) > eps && steps < STEPS_MAX); 
	// continue if not converged - otherwise stop
	// no more steps

	printf("Steps: %i\n",steps);
	printf("Function calls: %i\n",calls);
	printf("Function values (should be close to zero):\n");
	for(int i = 0; i < n; i++){
		int function_number = i + 1;
		printf("\tf%i(x,y) = "FMT"\n", function_number, gsl_vector_get(fx,i));
	}

	// free memory
	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_vector_free(Dx);
	gsl_vector_free(xx);
	gsl_vector_free(fxx);
}

void newton_refined(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double eps){
	int n = x->size, steps = 0, calls = 0;

	// allocate memory
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_vector* xx = gsl_vector_alloc(n);
	gsl_vector* fxx = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	
	f(x,fx,J); // void eq_system_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);
	calls++;

	do{ // take a step
		// Newton's step: solve J*Dx = -fx for Dx through QR decomposition of J
		gsl_matrix_memcpy(Q,J);
		qr_decomposition(Q,R); 
		qr_solve(Q,R,fx,Dx);
		gsl_vector_scale(Dx,-1); // Dx result

		// refined line search: minimize g(lambda) = = 0.5*||f(x + lambda*Dx)||²
		double g0 = 0.5*pow(gsl_blas_dnrm2(fx),2); // g(0)
		double dg0 = -pow(gsl_blas_dnrm2(fx),2); // g'(0)
		double lambda = 1;
		gsl_vector_memcpy(xx,x); gsl_vector_add(xx,Dx); f(xx,fxx,J); // fxx = f(x + Dx)
		double g = 0.5*pow(gsl_blas_dnrm2(fxx),2); // g(1) = 0.5*||f(x + Dx)||²
		double c = (g - g0 - dg0*lambda)/(lambda*lambda);
		int ntimes = 0;
		do{
			ntimes++;
			lambda = -0.5*dg0/c; 
			gsl_vector_memcpy(xx,x); // copy x and make changes on the copy - the original x remains unchanged
			// memcpy is needed because of the if-statement further down
			gsl_vector_scale(Dx,lambda); // Dx <- lambda*Dx
			gsl_vector_add(xx,Dx); // walk Newton's step: xx <- xx + Dx
			f(xx,fxx,J); // fill fxx with function values;
			calls++;
			if(gsl_blas_dnrm2(fxx) < (1 - lambda/2)*gsl_blas_dnrm2(fx) || ntimes > NTIMES_MAX) break;	
			// stop if ||f(x + lambda*Dx)|| < (1 - lambda/2) ||f(x)|| OR if lambda is too small
			// otherwise continue: modify g and c
			g = 0.5*pow(gsl_blas_dnrm2(fxx),2); // g(lambda) = 0.5*||f(x + lambda*Dx)||²
			c = (g - g0 - dg0*lambda)/(lambda*lambda);
		}while(calls < CALLS_MAX);

		// save result - get ready for next step
		gsl_vector_memcpy(x,xx); // x <- xx
		gsl_vector_memcpy(fx,fxx); // fx <- fxx
		steps++;
	} while(gsl_blas_dnrm2(fx) > eps && steps < STEPS_MAX); 
	// continue if not converged - otherwise stop
	// no more steps

	printf("Steps: %i\n",steps);
	printf("Function calls: %i\n",calls);
	printf("Function values (should be close to zero):\n");
	for(int i = 0; i < n; i++){
		int function_number = i + 1;
		printf("\tf%i(x,y) = "FMT"\n", function_number, gsl_vector_get(fx,i));
	}

	// free memory
	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_vector_free(Dx);
	gsl_vector_free(xx);
	gsl_vector_free(fxx);
}
