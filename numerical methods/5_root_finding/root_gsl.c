#include"header.h"
#include<gsl/gsl_multiroots.h>

void root_gsl(int f(const gsl_vector* x, void * params, gsl_vector* fx, gsl_matrix * J), gsl_vector * x, double eps){
	// finds the minimum of the function, f, by finding the root of it's gradient 
	// that is, the point where the gradient is zero
	int n = 2, status, iter = 0;
	// multiroot function
	gsl_multiroot_function_fdf multiroot_function;
	multiroot_function.fdf = f;
	multiroot_function.n = n;
	multiroot_function.params = NULL;
	// initialize solver
	const gsl_multiroot_fdfsolver_type *solver_type = gsl_multiroot_fdfsolver_newton;
	gsl_multiroot_fdfsolver *solver = gsl_multiroot_fdfsolver_alloc (solver_type, n);
	gsl_multiroot_fdfsolver_set(solver, &multiroot_function, x);
	// solve
	do{
	  iter++;
	  status = gsl_multiroot_fdfsolver_iterate(solver);
	  if(status)break;// check if solver is stuck
	  status = gsl_multiroot_test_residual(solver->f, eps);
	}
	while (status == GSL_CONTINUE && iter < 1000);
	//printf("status = %s\n", gsl_strerror(status));
	// result
	gsl_vector_memcpy(x,solver->x);
	printf("Iterations: %i\n",iter);
	printf("Function values (should be close to zero):\n");
	for(int i = 0; i < n; i++){
		int function_number = i + 1;
		printf("\tf%i(x,y) = "FMT"\n", function_number, gsl_vector_get(solver->f,i));
	}
	// free memory
	gsl_multiroot_fdfsolver_free(solver);
}