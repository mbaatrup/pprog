#include"header.h"

int main(){ 
	// allocate memory
	int n = 2; 
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);

	// accuracy goal, minimum stepsize, and start points
	double eps = 1e-3, dx = 1e-6;
	double x0_eq_sys = 0, y0_eq_sys = 10;
	double x0_rosenbrock = 0, y0_rosenbrock = 0;
	double x0_himmelblau = 0, y0_himmelblau = 0;

	printf("-----------------------------------------------------------------------\n");
	printf("System of equations: A*x*y = 1, exp(-x) + exp(-y) = 1 + 1/A");
	printf("\n-----------------------------------------------------------------------\n");

	printf("\nNewton's method with back-tracking linesearch and numerical Jacobian\n");
	gsl_vector_set(x,0,x0_eq_sys);
	gsl_vector_set(x,1,y0_eq_sys);
	newton(eq_system, x, dx, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Minimium step size: dx = %.1e \n", dx);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_eq_sys, y0_eq_sys);
	printf("Root: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with back-tracking linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_eq_sys);
	gsl_vector_set(x,1,y0_eq_sys);
	newton_with_jacobian(eq_system_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_eq_sys, y0_eq_sys);
	printf("Root: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with GSL multiroot\n");
	gsl_vector_set(x,0,x0_eq_sys);
	gsl_vector_set(x,1,y0_eq_sys);
	root_gsl(eq_system_with_jacobian_gsl, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_eq_sys, y0_eq_sys);
	printf("Root: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with refined linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_eq_sys);
	gsl_vector_set(x,1,y0_eq_sys);
	newton_refined(eq_system_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_eq_sys, y0_eq_sys);
	printf("Root: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\n-----------------------------------------------------------------------\n");
	printf("Rosenbrock valley function: (1 - x)² + 100*(y - x²)²");
	printf("\n-----------------------------------------------------------------------\n");
	
	printf("\nNewton's method with back-tracking linesearch and numerical Jacobian\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	newton(rosenbrock, x, dx, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Minimium step size: dx = %.1e \n", dx);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\n--- B: Rosenbrock's valley function ---\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	//rosenbrock_with_jacobian(x, fx, J);
	newton_with_jacobian(rosenbrock_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with GSL multiroot\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	root_gsl(rosenbrock_with_jacobian_gsl, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_rosenbrock, y0_rosenbrock);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with refined linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	newton_refined(rosenbrock_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\n-----------------------------------------------------------------------\n");
	printf("Test function: (1 - x)² + 10*(y - x²)²");
	printf("\n-----------------------------------------------------------------------\n");
	
	printf("\nNewton's method with refined linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_rosenbrock);
	gsl_vector_set(x,1,y0_rosenbrock);
	newton_refined(rosenbrock_modified_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\n-----------------------------------------------------------------------\n");
	printf("Himmelblau's function: (x² + y - 11)² + (x + y² - 7)²");
	printf("\n-----------------------------------------------------------------------\n");

	printf("\nNewton's method with back-tracking linesearch and numerical Jacobian\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	newton(himmelblau, x, dx, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Minimium step size: dx = %.1e \n", dx);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with back-tracking linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	newton_with_jacobian(himmelblau_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\n Newton's method with GSL multiroot\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	root_gsl(himmelblau_with_jacobian_gsl, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	printf("\nNewton's method with refined linesearch and analytic Jacobian\n");
	gsl_vector_set(x,0,x0_himmelblau);
	gsl_vector_set(x,1,y0_himmelblau);
	newton_refined(himmelblau_with_jacobian, x, eps); 
	printf("Accuracy goal: eps = %.1e \n", eps);
	printf("Start point: (x0,y0) = (%g,%g)\n", x0_himmelblau, y0_himmelblau);
	printf("Extremum: (x,y) = ("FMT","FMT")\n", gsl_vector_get(x,0), gsl_vector_get(x,1));

	// -----------------------------------------------------------------------------------------

	// free memory
	gsl_vector_free(x);
	gsl_vector_free(fx);
	gsl_matrix_free(J);

	return 0;
}
