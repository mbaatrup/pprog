#include"header.h"

void eq_system(gsl_vector* coordinates, gsl_vector* function_values){ 
	// x vector of coordinates (x,y)
	// fx vector of function values f(x,y)
	// equations: f(x,y) = 0
	// 0) f(x,y) = A*x*y - 1
	// 1) f(x,y) = exp(-x) + exp(-y) - 1 - 1/A
	int A = 10000;
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	gsl_vector_set(function_values,0, A*x*y - 1);
	gsl_vector_set(function_values,1, exp(-x) + exp(-y) - 1 - 1/A);
}
void eq_system_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian){ 
	int A = 10000;
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	gsl_vector_set(function_values,0, A*x*y - 1);
	gsl_vector_set(function_values,1, exp(-x) + exp(-y) - 1 - 1/A);
	gsl_matrix_set(jacobian,0,0, A*y);
	gsl_matrix_set(jacobian,0,1, A*x);
	gsl_matrix_set(jacobian,1,0, -exp(-x));
	gsl_matrix_set(jacobian,1,1, -exp(-y));
}
int eq_system_with_jacobian_gsl(const gsl_vector* coordinates,  void *params, gsl_vector* function_values, gsl_matrix* jacobian){ 
	// params is not used for anything here, but it is needed in the syntax
	int A = 10000;
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	gsl_vector_set(function_values,0, A*x*y - 1);
	gsl_vector_set(function_values,1, exp(-x) + exp(-y) - 1 - 1/A);
	gsl_matrix_set(jacobian,0,0, A*y);
	gsl_matrix_set(jacobian,0,1, A*x);
	gsl_matrix_set(jacobian,1,0, -exp(-x));
	gsl_matrix_set(jacobian,1,1, -exp(-y));
	return GSL_SUCCESS;
}