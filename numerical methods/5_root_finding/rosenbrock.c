#include"header.h"

void rosenbrock(gsl_vector* coordinates, gsl_vector* function_values){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	gsl_vector_set(function_values,0, -2*(1 - x) - 400*(y - x*x)*x);
	gsl_vector_set(function_values,1, 200*(y - x*x));
}
void rosenbrock_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	// fill function_values
	gsl_vector_set(function_values,0, -2*(1 - x) - 400*(y - x*x)*x);
	gsl_vector_set(function_values,1, 200*(y - x*x) );
	// fill jacobian

	gsl_matrix_set(jacobian,0,0, 2 + 1200*x*x - 400*y);
	gsl_matrix_set(jacobian,0,1, - 400*x);
	gsl_matrix_set(jacobian,1,0, - 400*x);
	gsl_matrix_set(jacobian,1,1, 200);
}
int rosenbrock_with_jacobian_gsl(const gsl_vector* coordinates,  void *params, gsl_vector* function_values, gsl_matrix* jacobian){
	// params is not used for anything here, but it is needed in the syntax
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	// fill function_values
	gsl_vector_set(function_values,0, -2*(1 - x) - 400*(y - x*x)*x);
	gsl_vector_set(function_values,1, 200*(y - x*x));
	// fill jacobian
	gsl_matrix_set(jacobian,0,0, 2 + 1200*x*x - 400*y);
	gsl_matrix_set(jacobian,0,1, - 400*x);
	gsl_matrix_set(jacobian,1,0, - 400*x);
	gsl_matrix_set(jacobian,1,1, 200);
	return GSL_SUCCESS;
}

void rosenbrock_modified_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian){
        double x = gsl_vector_get(coordinates,0);
        double y = gsl_vector_get(coordinates,1);
        gsl_vector_set(function_values,0, -2*(1 - x) - 40*(y - x*x)*x);
        gsl_vector_set(function_values,1, 20*(y - x*x) );
        gsl_matrix_set(jacobian,0,0, 2 + 120*x*x - 40*y);
        gsl_matrix_set(jacobian,0,1, - 40*x);
        gsl_matrix_set(jacobian,1,0, - 40*x);
        gsl_matrix_set(jacobian,1,1, 20);
}