#ifndef HEADER // if the header file is not already included, then include it
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h> 
#include<math.h>
#define FMT "%.6e"

// vectors and matrices
void print_vector(gsl_vector *v);
void print_matrix(gsl_matrix *M);
void qr_decomposition(gsl_matrix* A, gsl_matrix* R);
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

// non-linear equations
void eq_system(gsl_vector* coordinates, gsl_vector* function_values);
void eq_system_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);
int eq_system_with_jacobian_gsl(const gsl_vector* coordinates, void *params, gsl_vector* function_values, gsl_matrix* jacobian);

void rosenbrock(gsl_vector* coordinates, gsl_vector* function_values);
void rosenbrock_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);
int rosenbrock_with_jacobian_gsl(const gsl_vector* coordinates, void *params, gsl_vector* function_values, gsl_matrix* jacobian);
void rosenbrock_modified_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);

void himmelblau(gsl_vector* coordinates, gsl_vector* function_values);
void himmelblau_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian);
int himmelblau_with_jacobian_gsl(const gsl_vector* coordinates, void *params, gsl_vector* function_values, gsl_matrix* jacobian);

// Newton's method for root finding
void newton(void f(gsl_vector* x, gsl_vector* fx), gsl_vector* xstart, double dx, double eps);
void newton_with_jacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double eps);
void newton_refined(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double eps);

// gsl root finding
void root_gsl(int f(const gsl_vector* x, void* params, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double eps);

#define HEADER // the header file is included and will not be included again
#endif