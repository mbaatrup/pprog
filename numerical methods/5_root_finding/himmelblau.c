#include"header.h"
void himmelblau(gsl_vector* coordinates, gsl_vector* function_values){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	gsl_vector_set(function_values,0, 4*x*(x*x + y - 11) + 2*(x + y*y - 7));
	gsl_vector_set(function_values,1, 2*(x*x + y - 11) + 4*y*(x + y*y - 7));
}
void himmelblau_with_jacobian(gsl_vector* coordinates, gsl_vector* function_values, gsl_matrix* jacobian){
	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	// fill function_values
	gsl_vector_set(function_values,0, 4*x*(x*x + y - 11) + 2*(x + y*y - 7));
	gsl_vector_set(function_values,1, 2*(x*x + y - 11) + 4*y*(x + y*y - 7));
	// fill jacobian
	gsl_matrix_set(jacobian,0,0, 12*x*x + 4*y - 42);
	gsl_matrix_set(jacobian,0,1, 4*x + 4*y);
	gsl_matrix_set(jacobian,1,0, 4*x + 4*y);
	gsl_matrix_set(jacobian,1,1, 4*x + 12*y*y - 26);
}
int himmelblau_with_jacobian_gsl(const gsl_vector* coordinates,  void *params, gsl_vector* function_values, gsl_matrix* jacobian){
	// params is not used for anything here, but it is needed in the syntax

	double x = gsl_vector_get(coordinates,0);
	double y = gsl_vector_get(coordinates,1);
	// fill function_values
	gsl_vector_set(function_values,0, 4*x*(x*x + y - 11) + 2*(x + y*y - 7));
	gsl_vector_set(function_values,1, 2*(x*x + y - 11) + 4*y*(x + y*y - 7));
	// fill jacobian
	gsl_matrix_set(jacobian,0,0, 12*x*x + 4*y - 42);
	gsl_matrix_set(jacobian,0,1, 4*x + 4*y);
	gsl_matrix_set(jacobian,1,0, 4*x + 4*y);
	gsl_matrix_set(jacobian,1,1, 4*x + 12*y*y - 26);

	return GSL_SUCCESS;
}