#include"header.h"
#define RND ((double) rand()/RAND_MAX)

void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x){ // filling x with random numbers between a and b
	int n = a->size;
	for(int j = 0; j < n; j++){
		double xj = gsl_vector_get(a,j) + RND *(gsl_vector_get(b,j) - gsl_vector_get(a,j));
		gsl_vector_set(x,j, xj);
	}
}

void plain_monte_carlo(double f(gsl_vector* x), gsl_vector* a, gsl_vector* b, int N, double* Q, double* err){
	int n = a->size;
	// integration volume
	double V = 1; 
	for(int j = 0; j < n; j++){
		V *= gsl_vector_get(b,j) - gsl_vector_get(a,j);
	}
	// integration points and values
	double fx, sum_fx = 0, sum_fx_squared = 0;
	gsl_vector* x = gsl_vector_alloc(n);
	for(int i = 0; i < N; i++){
		random_x(a, b, x); // filling x with random numbers between a and b
		fx = f(x); 
		sum_fx += fx;
		sum_fx_squared += fx*fx;
	}
	gsl_vector_free(x);
	// variance, integral estimate, error
	double var = sum_fx_squared/N - pow(sum_fx/N,2);
	*Q = V*sum_fx/N;
	*err = V*sqrt(var/N);
}
