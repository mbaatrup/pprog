#ifndef HEADER 
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_integration.h>

void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x);
void plain_monte_carlo(double f(gsl_vector* x), gsl_vector* a, gsl_vector* b, int N, double* Q, double* err);

#define HEADER 
#endif
