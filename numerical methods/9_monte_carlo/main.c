#include"header.h"
#define FMT "%.7e"

double integrand_1(gsl_vector* xyz){
	double x = gsl_vector_get(xyz,0);
	double y = gsl_vector_get(xyz,1);
	double z = gsl_vector_get(xyz,2);
	return pow(x,2) + pow(y,2) + pow(z,2);// x² + y² + z²
}
double integrand_2(gsl_vector* spherical_coordinates){
	double r = gsl_vector_get(spherical_coordinates,0);
	double theta = gsl_vector_get(spherical_coordinates,1);
	return r*r*sin(theta);
}
double integrand_3(gsl_vector* xyz){
	double x = gsl_vector_get(xyz,0);
	double y = gsl_vector_get(xyz,1);
	double z = gsl_vector_get(xyz,2);
	return 1/( pow(M_PI,3)*(1 - cos(x)*cos(y)*cos(z)) );
}

int main(){ 
	printf("Plain Monte Carlo integration\n");
	int N, N_max = 1e5; // number of integration points, maximum number of integration points
	double I, Q, err; // exact result, integral estimate, error estimate
	
	// allocate memory
	int n = 3;
	gsl_vector* a = gsl_vector_alloc(n);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_vector* x = gsl_vector_alloc(n);

	printf("\nIntegrand: x² + y² + z²\n");
	// limits
	for(int j = 0; j < n; j++){
		gsl_vector_set(a,j,0); 
		gsl_vector_set(b,j,1);
	}
	// exact result
	I = 1.0; 
	// integration
	N = N_max;
	plain_monte_carlo(integrand_1, a, b, N, &Q, &err);
	printf("Lower limits: a = (0,0,0)\n");
	printf("Upper limits: b = (1,1,1)\n");
	printf("Number of integration points: N = %i\n", N);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));
	// error
	fprintf(stderr,"N \terr \n");
	double NN_min = 10, NN_max = 1e3, NN_steps = (NN_max - NN_min)/1e3;
	for(double NN = NN_min; NN < NN_max; NN += NN_steps){
		plain_monte_carlo(integrand_1, a, b, NN, &Q, &err);
		fprintf(stderr,"%g \t%8g \n", NN, err);
	}
 
	printf("\nIntegrand: r² sin(theta)\n");
	// limits
	for(int j = 0; j < n; j++){
		gsl_vector_set(a,j,0);
	} // lower limits
	gsl_vector_set(b,0,1); // r upper limit
	gsl_vector_set(b,1,M_PI); // phi upper limit
	gsl_vector_set(b,2,2.0*M_PI); // theta upper limit
	// exact result
	I = 4.0*M_PI/3.0;
	// integration
	N = N_max;
	plain_monte_carlo(integrand_2, a, b, N, &Q, &err);
	printf("Lower limits: a = (0,0,0)\n");
	printf("Upper limits: b = (1,π,2π)\n");
	printf("Number of integration points: N = %i\n", N);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));

	printf("\nIntegrand: [π³(1 - cos(x)cos(y)cos(z))]⁻¹\n");
	// limits
	for(int j = 0; j < n; j++){
		gsl_vector_set(a,j,0); 
		gsl_vector_set(b,j,M_PI);
	}
	// exact result
	I =  pow(tgamma(1.0/4),4) / (4.0*pow(M_PI,3));
	// integration
	N = N_max;
	plain_monte_carlo(integrand_3, a, b, N, &Q, &err);
	printf("Lower limits: a = (0,0,0)\n");
	printf("Upper limits: b = (π,π,π)\n");
	printf("Number of integration points: N = %i\n", N);
	printf("Exact result: I = "FMT"\n", I);
	printf("Estimated result: Q = "FMT"\n", Q);
	printf("Estimated error: err = "FMT"\n", err);
	printf("Deviation: |I - Q| = "FMT"\n", fabs(I - Q));

	// free memory
	gsl_vector_free(a);
	gsl_vector_free(b);
	gsl_vector_free(x);
	
	return 0;
}


