#ifndef HAVE_NVECTOR_H // if HAVE_NVECTOR_H is not defined then do the following

typedef struct nvector{int size; double* data;} nvector; // defining a new structure called nvector and make typing shorter

nvector* nvector_alloc       (int n); 					// allocates memory for size-n vector 
void     nvector_free        (nvector* v);              // frees memory
void     nvector_set         (nvector* v, int i, double value); // sets the value of the ith element
double   nvector_get         (nvector* v, int i);       // returns the value of the ith element
double   nvector_dot_product (nvector* u, nvector* v);  // returns the dot-product

#define HAVE_NVECTOR_H
#endif