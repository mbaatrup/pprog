#include<stdio.h>	// needed for printf and fprintf
#include<stdlib.h> 	// needed for malloc and NULL
#include<math.h>	// needed for math stuff
#include<assert.h>	// needed for assert()
#include"nvector.h"	// contains all nvector function declarations

nvector* nvector_alloc(int n){			// allocating memory for a vector of length (size) n
	nvector* v = malloc(sizeof(nvector)); // initializing an instance of the nvector structure type
    (*v).size = n;						// accessing the members of the structure using dot-notation
    (*v).data = malloc(n*sizeof(double)); // allocating an amount of memory corresponding to the size of n doubles.
    if(v == NULL) 
  		fprintf(stderr,"error in nvector_alloc\n"); // error massage in case the vector is empty
    return v;
}

void nvector_free(nvector* v){ 			// freeing the memory allocated to a vector
	free((*v).data); 					
	free(v);
}

void nvector_set(nvector* v, int i, double value){ // setting the ith value of the vector
	(*v).data[i]=value;  				// the ith element of the data elements of the poiner to vector v is set equal to value
}

double nvector_get(nvector* v, int i){
	return (*v).data[i]; 				// returning the ith element of the data elements of the pointer to vector v
}

double nvector_dot_product(nvector* v, nvector* u){
	double result;
	if((*v).size == (*u).size){		// checking if the two vectors have the same size
		for(int i = 0; i < (*v).size; i++){
			result += nvector_get(v,i)*nvector_get(u,i); // multiply element by element and sum all the products
		}
	}
	else printf("The vectors do not have the same size.\n");
	return result;
}