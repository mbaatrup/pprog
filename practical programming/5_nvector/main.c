#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include"nvector.h"

int main(){

	int n = 5;

	printf("nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("nvector_alloc works \n");

	printf("\nnvector_set and nvector_get ... \n");
	for (int i = 0; i < (*v).size; i++){
		nvector_set(v, i, i);
		printf("v[%i] = %g\n", i, nvector_get(v, i));
	}

	printf("\nnvector_dot_product ...\n");
	printf("v dot v = %g\n", nvector_dot_product(v, v));

	nvector_free(v);
	return 0;
}
