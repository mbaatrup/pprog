#include<stdio.h>
#include<math.h>
#include<limits.h>
#include<float.h>

int equal(double a, double b, double tau, double epsilon);

int main(void)
{
// [1]
	printf("\nMaximum integers:\n");
	int i = 1;
	while(i + 1 > i){i++;}
	printf("while max int = %i\n",i);

	for(i = 1; i + 1 > i; i++);
	printf("for max int = %i\n",i);

	do{i++;} 
	while(i + 1 > i);
	printf("do while max int = %i\n",i);

	printf("limits.h INT_MAX = %i\n",INT_MAX);


	printf("\nMinimum integers:\n");
	while(i - 1 < i){i++;}
	printf("while min int = %i\n",i);

	for(i = 1 ; i - 1 < i; i++){}
	printf("for min int = %i\n",i);

	do{i++;} 
	while(i - 1 < i);
	printf("do while min int = %i\n",i); 

	printf("limits.h INT_MIN = %i\n",INT_MIN);


	printf("\nMachine epsilon:\n");
	double wd = 1;
	while(1 + wd != 1){wd /= 2;} 
	wd *= 2;
	printf("while double epsilon = %g\n",wd);

	double fd;
	for(fd = 1; 1 + fd != 1; fd /= 2){}
	fd *= 2;
	printf("for double epsilon = %g\n",fd);

	double dd = 1;
	do{dd /= 2;}
	while(1 + dd != 1);
	dd *= 2;
	printf("do while double epsilon = %g\n",dd);

	printf("float.h DBL_EPSILON = %g\n",DBL_EPSILON);

	float wf = 1;
	while(1 + wf != 1){wf /= 2;} 
	wf *= 2;
	printf("while float epsilon = %f\n",wf);

	float ff;
	for(ff = 1; 1 + ff != 1; ff /= 2){}
	ff *= 2;
	printf("for float epsilon = %f\n",ff);

	float df = 1;
	do{df /= 2;}
	while(1 + df != 1);
	df *= 2;
	printf("do while float epsilon = %g\n",df);

	printf("float.h FLT_EPSILON = %f\n",FLT_EPSILON);

	long double wl = 1;
	while(1 + wl != 1){wl /= 2;} 
	wl *= 2;
	printf("while long double epsilon = %Lg\n",wl);

	long double fl;
	for(fl = 1; 1 + fl != 1; fl /= 2){}
	fl *= 2;
	printf("for long double epsilon = %Lg\n",fl);

	long double dl = 1;
	do{dl /= 2;}
	while(1 + dl != 1);
	dl *= 2;
	printf("do while double epsilon = %Lg\n",dl);

	printf("float.h LDBL_EPSILON = %Lg\n",LDBL_EPSILON);


// [2]
	printf("\nCalculating sums:\n");

	int max = INT_MAX/2;
	printf("max = %i\n",max);

	float sum_up_float, sum_down_float;
	for(i = 1; i <= max; i++){
		sum_up_float += 1.0f/i; // sum over 1/n with n = 1,2,...
		sum_down_float += 1.0f/(max - i + 1); // sum over 1/n with n = max,max-1,...
	}
	printf("sum up float = %f\nsum down float = %f\n",sum_up_float,sum_down_float);
	printf("The two results differ, because the for loop rounds off the two different results for every iteration.\n");
	printf("The sum is a part of the harmonic series, which is divergent.\n");

	double sum_up_double, sum_down_double;
	for(i = 1; i <= max; i++){
		sum_up_double += 1.0/i;
		sum_down_double += 1.0/(max - i + 1);
	}
	printf("sum up double = %g\nsum down double = %g\n",sum_up_double,sum_down_double);
	printf("Using double, the two results appear equal, because the round off errors are small.\n");


// [3]
	printf("\nequal function:\n");

	double a = 1.00, b = 0.90, tau = 0.01, epsilon = 0.001;
	printf("tau = %g, epsilon = %g\n",tau,epsilon); 

	int result = equal(a, b, tau, epsilon);
	printf("Returns 1 if the input numbers are equal with absolute precision tau or relative precision epsilon.\nReturns 0 otherwise.\n");
	printf("Test: Is %g = %g? \nReturn value: %i\n",a,b,result);

	return 0;
}