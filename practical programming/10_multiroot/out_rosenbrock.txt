Rosenbrock function: f(x,y) = (1-x)² + 100(y-x²)²
Minimum: (x,y) = (1.000000035,1.000000071)
Initial guess: (x0,y0) = (1.1,1.1)
Number of iterations: 38
