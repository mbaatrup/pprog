#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>

struct rparams{double a; double b;};

// Multiroot function. Syntax: int (* f) (const gsl_vector * x, void * params, gsl_vector * f)
int rosenbrock_function(const gsl_vector* r, void* params, gsl_vector*f){ // f(x,y)=(a-x)^{2}+b(y-x^{2})^{2}
	double x = gsl_vector_get(r,0);
	double y = gsl_vector_get(r,1);
	double a = ((struct rparams*)params) -> a;
	double b = ((struct rparams*)params) -> b;

	double dfdx = -2*(a - x) - 4*b*x*(y - (x*x));
	double dfdy = 2*b*(y - (x*x));

	gsl_vector_set(f,0,dfdx); 
	gsl_vector_set(f,1,dfdy); 
	return GSL_SUCCESS;
}

int main(){
// Initialization
	const gsl_multiroot_fsolver_type* solver_type = gsl_multiroot_fsolver_hybrids;
	size_t system_dimension = 2;
	gsl_multiroot_fsolver* solver = gsl_multiroot_fsolver_alloc(solver_type, system_dimension);

// Parameters
	double a = 1.0, b = 100;
	struct rparams parameters = {a, b};

// Multiroot function
	gsl_multiroot_function multiroot_function = {&rosenbrock_function, system_dimension, (void *) &parameters};

// Initial guess
	double x0 = 1.1, y0 = 1.1;
	gsl_vector* initial_guess = gsl_vector_alloc(2);
	gsl_vector_set(initial_guess, 0, x0);
	gsl_vector_set(initial_guess, 1, y0);
	gsl_multiroot_fsolver_set(solver, &multiroot_function, initial_guess);

// Iterations
	double epsabs = 1e-6;
	int status, iter = 0, iter_max = 1e2;
	fprintf(stderr,"x \t y\t iteration\n");
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(solver); // do one iteration
		status = gsl_multiroot_test_residual(solver->f, epsabs); // f is a vector of residual values

		double x = gsl_vector_get(solver->x, 0), y = gsl_vector_get(solver->x, 1);
		fprintf(stderr,"%.10g \t %.10g \t %i\n", x, y, iter);

		if(status == GSL_SUCCESS)break;
	}while(status == GSL_CONTINUE && iter < iter_max); // continue if solver wants to and the iteration number is not too big

// Result
	double x_result = gsl_vector_get(solver->x, 0), y_result = gsl_vector_get(solver->x, 1);
	printf("Rosenbrock function: f(x,y) = (1-x)² + 100(y-x²)²\n");
	printf("Minimum: (x,y) = (%.10g,%.10g)\n", x_result, y_result);
	printf("Initial guess: (x0,y0) = (%.10g,%.10g)\n", x0, y0);
	printf("Number of iterations: %i\n", iter);

// Free memory
	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(initial_guess);

	return 0;
}