#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<assert.h>
#define R_MIN 1e-3
#define R_MAX 10

// s-wave radial Schrödinger equation for the Hydrogen atom
int schrodinger_eq(double r, const double f[], double dfdr[], void* params){
//-(1/2)f'' -(1/r)f = εf ,
	double eps = *(double*)params;
	dfdr[0] = f[1];
	dfdr[1] = -2*f[0]*(eps + 1/r);
	return GSL_SUCCESS;
}

// Solving differential equation "schrodinger_eq"
double wavefunction(double eps,double r){
// Limits
	const double r_min = R_MIN;				// minimum position, close to zero

// Make sure that r is larger than zero. Use initial condition if r is smaller than the chosen minimum.
	assert(r >= 0);
	if(r < r_min) return r - r*r;

// Defining differential system
	gsl_odeiv2_system hydrogen_sys = {schrodinger_eq, NULL, 2, (void*)&eps}; // {function, jacobian, dimension, params}

// Setting up the driver for solving the differential system
	const gsl_odeiv2_step_type* steptype = gsl_odeiv2_step_rk8pd; // steptype
	double hstart = 1e-3;					// initial step size
	double epsabs = 1e-6;					// absolute error
	double epsrel = 1e-6;					// relative error
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&hydrogen_sys, steptype, hstart, epsabs, epsrel);

// Initial conditions				
	double r_i = r_min;
	double f_i = r_i - r_i*r_i;
	double dfdr_i = 1 - 2*r_i;
	double y[2] = {f_i,dfdr_i};

// Solving the differential system (in one go)	
	int status = gsl_odeiv2_driver_apply(driver,&r_i,r,y);
	if(status != GSL_SUCCESS) 
		fprintf(stderr,"hydrogen.c: odeiv2 error = %i",status);

// Free memory and return result
	gsl_odeiv2_driver_free(driver);
	return y[0];
}
// Function for multiroot
// Syntax: int (* f) (const gsl_vector * x, void * params, gsl_vector * f)
int auxiliary_function(const gsl_vector* variable_energy, void* constant_rmax, gsl_vector* variable_wavefunction){
	double r_max = *(double*)constant_rmax;
	double energy = gsl_vector_get(variable_energy, 0);
	double wavefunction_r_max = wavefunction(energy, r_max);
	gsl_vector_set(variable_wavefunction, 0, wavefunction_r_max);
	return GSL_SUCCESS;
}

int main(){
	double r_min = R_MIN, r_max = R_MAX;

// Initialization
	const gsl_multiroot_fsolver_type* solver_type = gsl_multiroot_fsolver_hybrids;
	size_t system_dimension = 1;
	gsl_multiroot_fsolver* solver = gsl_multiroot_fsolver_alloc(solver_type, system_dimension);

// Multiroot function
// Syntax: gsl_multiroot_function multiroot_function = {&function, size_t system_dimension, (void*)&params};
	gsl_multiroot_function multiroot_function = {&auxiliary_function, system_dimension, (void*)&r_max};

// Initial guess
	double eps_0 = -1.0;
	gsl_vector* initial_guess = gsl_vector_alloc(1);
	gsl_vector_set(initial_guess, 0, eps_0);
	// Syntax: int gsl_multiroot_fsolver_set(gsl_multiroot_fsolver * s, gsl_multiroot_function * f, const gsl_vector * x)
	gsl_multiroot_fsolver_set(solver, &multiroot_function, initial_guess);

// Iterations
	double epsabs = 1e-6;
	int status, iter = 0, iter_max = 1e2;
	fprintf(stderr,"energy \titeration\n");
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(solver); // do one iteration
		status = gsl_multiroot_test_residual(solver->f, epsabs); // f is a vector of residual values

		double energy = gsl_vector_get(solver->x, 0);
		fprintf(stderr,"%.10g \t%i\n",energy,iter);
		if(status == GSL_SUCCESS)break;
	}while(status == GSL_CONTINUE && iter < iter_max); // continue if solver wants to and the iteration number is not too big

// Result
	double energy_result = gsl_vector_get(solver->x, 0);
	printf("Hydrogen s-state energy\n");
	printf("r_max: %g\n", r_max);
	printf("Energy result: %10g\n", energy_result);
	printf("Energy initial guess: %g\n", eps_0);
	printf("Number of iterations: %i\n", iter);

// Data for plotting
	FILE* my_stream = fopen("data_wavefunction.txt","w");
	fprintf(my_stream, "r \twavefunction_numerical \twavefunction_analytical\n");
	double r, r_step = 0.1;
	for(r = r_min; r < r_max; r += r_step){
		double wavefunction_analytical = r*exp(-r);
		double wavefunction_numerical = wavefunction(energy_result, r);

		fprintf(my_stream, "%10g \t%10g \t%10g\n", r, wavefunction_numerical, wavefunction_analytical);
	}
	fclose(my_stream);

// Free memory
	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(initial_guess);

	return 0;
}
