#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
 
struct data_structure {int n; double *t,*y,*e;};

double least_squares_sum(const gsl_vector* fitting_parameters, void* experimental_data){
	double A = gsl_vector_get(fitting_parameters, 0);
	double T = gsl_vector_get(fitting_parameters, 1);
	double B = gsl_vector_get(fitting_parameters, 2);

	struct data_structure *data = (struct data_structure*) experimental_data;
	int n = data -> n;
	double *t = data -> t, *y = data -> y, *e = data -> e;

	#define MODEL(t) A*exp(-(t)/T)+B
	double sum = 0;
	for (int i = 0; i < n; i++){
		sum += pow((MODEL(t[i]) - y[i])/e[i], 2); // ∑_i(f(t_i)-y_i)²/σ_i².
	}
	return sum;
}

int main(){
	
// Data	
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]); // You need to divide with the size of one element - otherwise you'll get a number 8 times too large!

	printf("t \ty \te \n");
	for(int i = 0; i < n; i++){
		printf("%g \t%g \t%g \n", t[i], y[i], e[i]);
	}
	
	struct data_structure experimental_data;
	experimental_data.n = n;
	experimental_data.t = t;
	experimental_data.y = y;
	experimental_data.e = e;

// Multimin function
	gsl_multimin_function multimin_function;
	multimin_function.f = least_squares_sum;
	multimin_function.n = n;
	multimin_function.params = (void*)&experimental_data;

// Starting point
	double A0 = 4, T0 = 2, B0 = 1;
	gsl_vector *initial_guess = gsl_vector_alloc(n);
	gsl_vector_set(initial_guess,0,A0);
	gsl_vector_set(initial_guess,1,T0);
	gsl_vector_set(initial_guess,2,B0);

// Stepsize
	double step = 0.1;
	gsl_vector *stepsize = gsl_vector_alloc(n);
	gsl_vector_set_all(stepsize, step);

// Initialization
	const gsl_multimin_fminimizer_type *type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *solver = gsl_multimin_fminimizer_alloc(type, n);
	gsl_multimin_fminimizer_set(solver, &multimin_function, initial_guess,stepsize);

// Iterations
	int status, iter = 0, iter_max = 1e3; 
	double accuracy = 1e-4;
	fprintf(stderr,"A \t\tT \t\tB \t\tf(A,T,B) \titer\n");
	do{
		double A = gsl_vector_get(solver->x,0), 
			T = gsl_vector_get(solver->x,1),
			B = gsl_vector_get(solver->x,2),
			f = solver -> fval;
		fprintf(stderr,"%.6e \t%.6e \t%.6e \t%.6e \t%i \n", A, T, B, f, iter);

		iter++;
		status = gsl_multimin_fminimizer_iterate(solver);
		if(status)break;

		status = gsl_multimin_test_size(solver -> size, accuracy);
		if(status == GSL_SUCCESS){
			fprintf(stderr,"status = %s\n", gsl_strerror(status));
		}
	}
	while(status == GSL_CONTINUE && iter < iter_max);
	fprintf(stderr,"A \t\tT \t\tB \t\tf(A,T,B) \titer\n");
	
// Result
	double A= gsl_vector_get(solver -> x,0), 
		T = gsl_vector_get(solver -> x,1),
		B = gsl_vector_get(solver -> x,2),
		f = solver -> fval;
	fprintf(stderr,"\nLeast squares fit result:\n");
	fprintf(stderr,"A = %8g \tT = %8g \tB = %8g \tleast squares sum = %8g \n", A, T, B, f);
	fprintf(stderr,"Number of iterations: %i\n\n",iter);

	printf("\n\n");
	printf("t \ty_fit\n");
	double y_fit[n];
	for (int i = 0; i < n; i++){
		y_fit[i] = MODEL(t[i]);
		printf("%g \t%g \n", t[i], y_fit[i]);
	}

// Free memory
	gsl_vector_free(initial_guess);
	gsl_vector_free(stepsize);
	gsl_multimin_fminimizer_free(solver);

	return 0;
}
