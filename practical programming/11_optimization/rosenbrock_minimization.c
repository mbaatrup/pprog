#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h> 

double rosenbrock(const gsl_vector* v, void* params){
	double x = gsl_vector_get(v, 0);
	double y = gsl_vector_get(v, 1);

	double f = (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	return f;
}

int main(){
	
// Multimin function
	gsl_multimin_function multimin_function;
	int system_dimension = 2;
	multimin_function.f = rosenbrock;
	multimin_function.n = system_dimension;
	multimin_function.params = NULL;

// Starting point
	double x0 = -1, y0 = 1;
	gsl_vector* start = gsl_vector_alloc(system_dimension);
	gsl_vector_set(start, 0, x0);
	gsl_vector_set(start, 1, y0);

// Initial stepsize
	double initial_stepsize = 0.1;
	gsl_vector* step = gsl_vector_alloc(system_dimension);
	gsl_vector_set_all(step, initial_stepsize);

// Initialization
	const gsl_multimin_fminimizer_type* solver_type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer* solver = gsl_multimin_fminimizer_alloc(solver_type, system_dimension);
	gsl_multimin_fminimizer_set(solver, &multimin_function,start,step);

// Iterations	
	int status, iter = 0, iter_max = 1e3;
	double accuracy = 1e-4;
	fprintf(stderr,"x \t\ty \t\tf(x,y) \t\titer\n");
	do{
		double x = gsl_vector_get(solver -> x, 0), 
			y = gsl_vector_get(solver -> x, 1),
			f = solver -> fval;
		fprintf(stderr,"%.6e \t%.6e \t%.6e \t%i\n", x, y, f, iter);

		iter++;
		status = gsl_multimin_fminimizer_iterate(solver);
		if(status) break;

		status = gsl_multimin_test_size(solver->size, accuracy);
		if(status == GSL_SUCCESS) {
			fprintf(stderr,"status = %s\n", gsl_strerror(status));
		}
		
	}
	while(status == GSL_CONTINUE && iter < iter_max);
	fprintf(stderr,"x \t\ty \t\tf(x,y) \t\titer\n");

	double x_result = gsl_vector_get(solver->x, 0), 
		y_result = gsl_vector_get(solver->x, 1),
		f_result = solver->fval;
	fprintf(stderr,"\nRosenbrock minimization result:\n");
	fprintf(stderr,"x = %8g \t y = %8g \t f(x,y) = %8g \n", x_result, y_result, f_result);
	fprintf(stderr,"Number of iterations: %i\n\n", iter);


// Free memory
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(solver);

	return 0;
}
