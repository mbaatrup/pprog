#ifndef HEADER
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

int diff_eq(double x, const double y[], double dydx[], void* params);
double ode_ln(double x, int* calls);

#define HEADER
#endif