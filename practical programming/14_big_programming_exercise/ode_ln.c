#include"header.h"

int diff_eq(double x, const double y[], double dydx[], void* params){
	dydx[0] = 1/x;
	return GSL_SUCCESS;
}

double ode_ln(double x, int* calls){
	// function calls
	*calls += 1;

	// positive x
	assert(x > 0);

	// reducing x
	if(x < 1){return -ode_ln(1.0/x, calls);}
	if(x >= 2){return log(2.0) + ode_ln(x/2.0, calls);}

	// defining the ODE system
	gsl_odeiv2_system sys;
	sys.function = diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	// initialization: 
	double dx_start = 1e-3;
	double epsabs = 1e-6, epsrel = 1e-6;
	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, dx_start, epsabs, epsrel);

	// initial conditions
	double x0 = 1, y[1] = {0}; 

	// integrate
	int status = gsl_odeiv2_driver_apply(driver, &x0, x, y);
	if(status != GSL_SUCCESS) fprintf(stderr,"ode_ln: status = %i",status);

	// free memory
	gsl_odeiv2_driver_free(driver);

	return y[0];
}
