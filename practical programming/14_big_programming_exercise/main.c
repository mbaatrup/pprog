#include"header.h"

int main(int argc, char const *argv[]){	
	double a = atof(argv[1]), b = atof(argv[2]), dx = atof(argv[3]);

	int calls; // number of calls of ode_ln
	printf("x \ty \tln(x) \tcalls\n");

	for(double x = a; x < b; x += dx){
		calls = 0;
		double y = ode_ln(x, &calls);
		printf("%8g \t%8g \t%8g \t%i\n", x, y, log(x), calls);
	}
	printf("\n\n");

	printf("x \tcalls\n");
	for(double x = 1e-3; x < 3; x += 1e-3){
		calls = 0;
		double y = ode_ln(x, &calls);
		printf("%8g \t%8g \t%8g \t%i\n", x, y, log(x), calls);
	}

	return 0;
}
