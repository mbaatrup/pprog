#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_eq(double x, const double y[], double dydx[], void* params){
	dydx[0] = y[1];							// y is an array y = [y, y']
	dydx[0] = y[0]*(1 - y[0]);				// dydx = [y']
	return GSL_SUCCESS;
}

int main(){
	gsl_odeiv2_system diff_sys;
	diff_sys.function = diff_eq;
	diff_sys.jacobian = NULL;
	diff_sys.dimension = 1;
	diff_sys.params = NULL;

	const gsl_odeiv2_step_type* steptype 
		= gsl_odeiv2_step_rk8pd;
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;

	gsl_odeiv2_driver* driver 
		= gsl_odeiv2_driver_alloc_y_new(&diff_sys,steptype,hstart,epsabs,epsrel);

	double x, x0 = 0, x_max = 3, x_step = 0.1; 
	double y[1]; 							// declare array with one element
	y[0] = 0.5;								// set the first element to 0.5

	printf("x \t y\n");
	for(x = x0; x<x_max; x+=x_step){
		int status 							// gsl error codes: https://www.gnu.org/software/gsl/manual/html_node/Error-Codes.html
		= gsl_odeiv2_driver_apply(driver, &x0, x, y); // evolves the driver system from x1 to x
		printf("%g \t %g\n", x, y[0]);

		if(status != GSL_SUCCESS) 
			fprintf(stderr,"fun: status = %i",status); // if the evolution is not successful, print the gsl error code
	}

	gsl_odeiv2_driver_free(driver);

	return 0;
}