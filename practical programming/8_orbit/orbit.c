#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int orbit(double x, const double y[], double dydx[], void* params){ // u''(φ) + u(φ) = 1 + εu(φ)2 
	double eps = *(double*)params;
	dydx[0] = y[1];							// y' = y';
	dydx[1] = 1 + eps*y[0]*y[0] - y[0]; 	// y'' = 1 + eps*y^2 - y
	return GSL_SUCCESS;
}

int main(int argc,char** argv){
	double eps = 0,uprime=0;
		
	if (argc == 3){							// argc counts the number of elements in argv
		eps = atof(argv[1]), uprime = atof(argv[2]); // argv contains the program name and two values
	}

	gsl_odeiv2_system orbit_sys;
	orbit_sys.function = orbit;
	orbit_sys.jacobian = NULL;
	orbit_sys.dimension = 2;
	orbit_sys.params = (void*)&eps;

	const gsl_odeiv2_step_type* steptype 
		= gsl_odeiv2_step_rk8pd;
	double hstart = 1e-3;					// initial step size
	double epsabs = 1e-6;					// absolute error
	double epsrel = 1e-6;					// relative error

	gsl_odeiv2_driver* driver 
		= gsl_odeiv2_driver_alloc_y_new(&orbit_sys,steptype,hstart,epsabs,epsrel);

	double x, x0 = 0, x_max = 10*M_PI, x_step = 0.1; 
	double y[2]; 							// declare array with two element
	y[0] = 1;								// initial value y(0)
	y[1] = uprime;								// initial value y'(0)

	printf("x \t y\n");
	for(x = x0; x<x_max; x+=x_step){
		int status 							// gsl error codes: https://www.gnu.org/software/gsl/manual/html_node/Error-Codes.html
		= gsl_odeiv2_driver_apply(driver, &x0, x, y); // evolves the driver system from x0 to x
		printf("%g \t %g\n", x, y[0]);		// print the result for this step

		if(status != GSL_SUCCESS) 
			fprintf(stderr,"fun: status = %i",status); // if the evolution is not successful, print the gsl error code
	}

	gsl_odeiv2_driver_free(driver);

	return 0;
}