#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdlib.h>

double integrand(double x, void* params){
	double f = log(x)/sqrt(x);
	return f;
}

double H_integrand(double x, void* params){
	double alpha = *(double*) params;
	double f = (-pow(alpha,2)*pow(x,2)/2 + alpha/2 + pow(x,2)/2)*exp(-alpha*pow(x,2));
	return f;
}

double norm_integrand(double x, void* params){
	double alpha = *(double*) params;
	double f = exp(-alpha*pow(x,2));
	return f;
}

int main(void){
	size_t limit = 100;
	gsl_integration_workspace *ws = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = integrand;
	double a = 0, b = 1, acc = 1e-6, rel = 1e-6, res, err;

	gsl_integration_qags(&F, a, b, acc, rel, limit, ws, &res, &err);
	fprintf(stderr,"result = %.5f\terror = %g\n", res, err);

	double alpha;
	for (alpha = 0.1; alpha < 10; alpha += 0.1){
		double H_integral, H_err, norm_integral, norm_err;
		gsl_function H, N;
		H.function = H_integrand;
		H.params = (void*)&alpha;
		N.function = norm_integrand;
		N.params = (void*)&alpha;

		gsl_integration_qagi(&H, acc, rel, limit, ws, &H_integral, &H_err);
		gsl_integration_qagi(&N, acc, rel, limit, ws, &norm_integral, &norm_err);

		double E = H_integral/norm_integral;
		printf("%g\t%g\n",alpha,E);
	}
	
	gsl_integration_workspace_free(ws);

	return 0;
}
