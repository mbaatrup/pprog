#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_cblas.h>
#include<gsl/gsl_linalg.h>

void matrix_print(const char* s,const gsl_matrix* m){
	printf("\n%s\n",s);
	for(int j = 0; j < m->size2; j++){
		for(int i = 0; i < m->size1; i++)
			printf("%8.3g ",gsl_matrix_get(m, i, j));
		printf("\n");
	}
}
void vector_print(const char* s, const gsl_vector* v){
	printf("\n%s\n",s);
	gsl_vector_fprintf(stdout, v, "%g");
}

int main(){
	int n = 3;
	gsl_matrix* A = gsl_matrix_calloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_calloc(n,n);
	gsl_vector* x = gsl_vector_calloc(n);
	gsl_vector* b = gsl_vector_calloc(n);
	gsl_vector* Ax = gsl_vector_calloc(n);

	double A_data[] = {
		6.13, -2.90, 5.86,
		8.08, -6.31, -3.89,
		-4.36, 1.00, 0.19};
	double b_data[] = {
		6.23,
		5.37,
		2.29};

	for(int i = 0; i < n; i++){
		gsl_vector_set(b, i, b_data[i]);
		for(int j = 0; j < n; j++){
			gsl_matrix_set(A, i, j, A_data[n*i + j]);
		}
	}
	
	matrix_print("A =",A);
	vector_print("b =",b);

	// Copying A.
	gsl_matrix_memcpy(A_copy, A);
	// Householder solver for linear systems. 
	// A_copy is destroyed. Solution is stored in x.
	gsl_linalg_HH_solve(A_copy, b, x);

	printf("\nSolution:");
	vector_print("x = ", x);

	// Double GEneral Matrix Vector product. 
	// Computing: y = alpha*A*x + beta*y. Here, y = Ax, alpha = 1, beta = 0.
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, Ax);
	vector_print("Using gsl_blas_dgemv\nAx = ", Ax);

	printf("\nIs Ax = b? ");
	if(gsl_vector_equal(Ax, b)){
		printf("Yes!\n");
	}
	else{
		printf("No, not according to gsl_vector_equal.\n");
	}
	printf("... but it looks like they are equal, though.\n");

	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(Ax);

	return 0;
}