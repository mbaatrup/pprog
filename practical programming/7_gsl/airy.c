#include<stdio.h>
#include<gsl/gsl_sf_airy.h>

int main(){
	double x, Ai, Bi;
	printf("x \t Ai \t Bi \n");
	for(x = -15; x <= 5; x += 0.05){
		Ai = gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE);
		Bi = gsl_sf_airy_Bi(x,GSL_PREC_DOUBLE);
		printf("%g \t %g \t %g \n", x, Ai, Bi);
	}

	return 0;
}
