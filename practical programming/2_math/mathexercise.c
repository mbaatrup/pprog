#include <stdio.h>
#include <tgmath.h> // includes math.h, complex.h, and several type-generic macros

int main(void)
{
	double x = tgamma(5.0);
    printf("gamma(5) = %g\n", x);

    x = j1(0.5);
    printf("J1(0.5) = %g\n", x);

    complex z = csqrt(-2);
    printf("sqrt(-2) = %g + i%g\n", creal(z), cimag(z));

    z = exp(I);
    printf("exp(i) = %g + i%g\n", creal(z), cimag(z));

    z = exp(I*M_PI);
    printf("exp(i*pi) = %g + i%g\n", creal(z), cimag(z));

    z = pow(I,M_E);
    printf("i^e = %g + i%g\n", creal(z), cimag(z));

    double a = 0.1111111111111111111111111111;
    float b = 0.1111111111111111111111111111f;
    long double c = 0.1111111111111111111111111111L;

    printf("double x = %.25g\n", a);
    printf("float x = %.25g\n", b);
    printf("long double x = %.25Lg\n", c);

    return 0;
}