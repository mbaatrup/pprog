#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_eq(double x, const double y[], double dydx[], void* params){
	dydx[0] = y[1];
	dydx[0] = 2/sqrt(M_PI)*exp(-x*x);
	return GSL_SUCCESS;
}

double numerical_erf(double x){
	// differential equation system
	gsl_odeiv2_system sys;
	sys.function = diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	// initialization
	double dx_start = copysign(0.1, x), epsabs = 1e-6, epsrel = 1e-6;
	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, dx_start, epsabs, epsrel);

	// initial conditions
	double x0 = 0, y[1] = {0}; 

	int status = gsl_odeiv2_driver_apply(driver, &x0, x, y);
	if(status != GSL_SUCCESS) fprintf(stderr,"numerical_erf: status = %i",status);

	// free memory
	gsl_odeiv2_driver_free(driver);

	return y[0];
}

int main(int argc, char const *argv[]){	
	// input from Makefile
	double a = atof(argv[1]), b = atof(argv[2]), dx = atof(argv[3]); 
	
	printf("x \ty \terf(x)\n");
	for(double x = a; x < b; x += dx){
		printf("%g \t%g \t%g\n", x, numerical_erf(x), erf(x));
	}

	return 0;
}
