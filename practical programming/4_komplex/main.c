#include<stdio.h>
#include"komplex.h"

//#define TINY 1e-6


int main(){
/* komplex_print, komplex_add, komplex_equal */
	komplex a = {1, 2}, b = {2, 3}; /* a = 1 + i*2*/
	komplex_print("komplex_print: a =", a);
	komplex_print("komplex_print: b =", b);

	komplex r = komplex_add(a, b);
	komplex_print("komplex_add: a + b =", r);
	komplex R = {3, 5};
	double TINY = 1e-6;

	if(komplex_equal(r, R, TINY, TINY))
		printf("komplex_add test: It works.\n");
	else
		printf("komplex_add test: It doesn't work.\n");

	komplex tiny = {TINY,TINY};
	komplex c = komplex_add(a, tiny);
	komplex_print("komplex_add: a + tiny real number =", c); /* how can I include enough decimals? */

	if(	komplex_equal(a,c,TINY,TINY) )
		printf("komplex_equal: a = c? Yes\n");
	else
		printf("komplex_equal: a = c? No\n");

/* komprex_new, komplex_set */
	komplex z = komplex_new(4, 5);
	komplex_print("komplex_new: z =", z);
	komplex_set(&z, 5, 6);
	komplex_print("komplex_set: z =", z);

/* komplex_sub */
	komplex d = komplex_sub(a, b);
	komplex_print("komplex_sub: a - b =", d);

}