#include<stdio.h>
#include<math.h>
#include"komplex.h"


void komplex_print(char* s, komplex z){
	printf("%s (%g,%g)\n", s, z.re, z.im);
}

void komplex_set(komplex* z, double x, double y){
	(*z).re = x; /* take the real part of the value that z points to and call it x */
	(*z).im = y;
}

int komplex_equal(komplex a, komplex b, double tau, double epsilon){
	int result;
	if (fabs(a.re - b.re) < tau && fabs(a.im - b.im) < tau){
		result = 1;
	}
	else if (fabs(a.re - b.re)/(fabs(a.re) + fabs(b.re)) < epsilon/2 && fabs(a.im - b.im)/(fabs(a.im) + fabs(b.im)) < epsilon/2){
		result= 1;
	}
	else {
		result = 0;
	}
	return result;
}

komplex komplex_new(double x, double y){
	komplex z = {x,y};
	return z;
}

komplex komplex_add(komplex a, komplex b){
	komplex result = {a.re + b.re, a.im + b.im};
	return result;
}

komplex komplex_sub(komplex a, komplex b){
	komplex result = {a.re - b.re, a.im - b.im};
	return result;
}
