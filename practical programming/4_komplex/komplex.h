/* This header file declares new functions to deal with complex numbers. 
Header files should be included in the beginning of the files, where it's needed. */

#ifndef HAVE_KOMPLEX_H /* this is necessary for multiple includes */

struct komplex {double re; double im;};
typedef struct komplex komplex;

void komplex_print(char* s, komplex z); /* prints string s and complex number z */

void komplex_set(komplex* z, double x, double y); /* z is set to x + i*y */

int komplex_equal(komplex a, komplex b, double tau, double epsilon); /* returns 1 if equal, 0 otherwise */

komplex komplex_new(double x, double y); /* returns x + i*y */

komplex komplex_add(komplex a, komplex b); /* returns a + b */

komplex komplex_sub(komplex a, komplex b); /* returns a - b */

#define HAVE_KOMPLEX_H
#endif