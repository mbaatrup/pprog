#include<stdio.h> // needed for printf 
#include<stdlib.h> // needed for atof 
#include<math.h> // needed for sin 

int main(int argc, char** argv){ // equivalent to char* argv[]
	
	for(int i = 1; i < argc; i++){ // i starts at 1 because i = 0 is the name of the program
		double x = atof(argv[i]); // ascii to float
		printf("%lg \t %lg\n", x, sin(x)); // prints to stdout
	}

return 0;
}

// In terminal type for example: ./main-cmdline 1 2 3 4 5