#ifndef HEADER 
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#define FMT "%.3e"

typedef struct {int n; double (*g)(double); double(*dg)(double); gsl_vector* params;} ann; 

ann* ann_alloc(int n, double(*g)(double), double(*dg)(double));
void ann_free(ann* nw); 
void ann_feed_forward(ann* nw, double x, gsl_vector* F);
void ann_train(ann* nw, gsl_vector* x, double(*f)(double, double), double x0, double y0, double acc);
void ann_solve_ode(int n, gsl_vector* x, double(*f)(double,double), double x0, double y0, double acc);

int minimization(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, double acc);

#define HEADER
#endif
