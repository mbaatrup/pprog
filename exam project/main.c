#include"header.h" 

// right hand sides of first order differential equations (ODE)
double f_logistic(double x, double y){return y*(1 - y);}
double f_gaussian(double x, double y){return -x*y;}

int main(int argc, char const *argv[]){	
	int N = 100; // number of data points for training
	double a = -5, b = 5; // lower and upper limits of x
	double acc = 1e-3; // accuracy goal

	fprintf(stderr,"accuracy goal: acc = "FMT"\n", acc);

	// allocate memory
	gsl_vector* x = gsl_vector_alloc(N);

	// x values
	for(int k = 0; k < N; k++){
		double xk = a + (b - a)*(k - 1)/(N - 1);
		gsl_vector_set(x,k, xk);
	}

	// solve gaussian ODE for different number of hidden neurons
	fprintf(stderr,"\nANN solve gaussian ODE\n");
	for(int i = 1; i < argc; i++){
		int n = atof(argv[i]);
		fprintf(stderr,"\nnumber of hidden neurons: n = %i\n",n);
		ann_solve_ode(n, x, f_gaussian, 0.0, 1.0, acc);
	}
	fprintf(stderr,"\nremark: It appears that the deviation \ndecreases with the number of hidden neurons. \nHowever, the number of iterations increases a lot.\n");

	// solve logistic ODE
	fprintf(stderr,"\nANN solve logistic ODE\n");
	int n = 3;
	fprintf(stderr,"\nnumber of hidden neurons: n = %i\n",n);
	ann_solve_ode(n, x, f_logistic, 0.0, 0.5, acc);
	fprintf(stderr,"\nremark: In this case, 3 hidden neurons \nis enough to give an acceptable deviation. \n");

	// free memory
	gsl_vector_free(x);
	
	return 0;
}
