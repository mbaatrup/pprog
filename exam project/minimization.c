#include"header.h"
#include<gsl/gsl_multimin.h> 

int minimization(double(*deviation)(const gsl_vector*, void*), gsl_vector* p, double acc){
	int nn = p->size; // total number of parameters to be optimized

// multimin function
	gsl_multimin_function multimin_function;
	multimin_function.f = deviation;
	multimin_function.n = nn;
	multimin_function.params = NULL;

// initial step size
	gsl_vector *stepsize = gsl_vector_alloc(nn);
	gsl_vector_set_all(stepsize, 1);

// initialization
	gsl_multimin_fminimizer *solver = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, nn);
	gsl_multimin_fminimizer_set(solver, &multimin_function, p, stepsize);

// iterations	
	int status, iter = 0, iter_max = 1e5;
	
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(solver);
		if(status){
			fprintf(stderr,"minimization: iterate status = %s\n", gsl_strerror(status));
			break;
		}

		status = gsl_multimin_test_size(solver->size, acc);
		if(status == GSL_SUCCESS){
			fprintf(stderr,"minimization: status = %s\n", gsl_strerror(status));
		}
	}
	while(status == GSL_CONTINUE && iter < iter_max);
	
// result
	gsl_vector_memcpy(p, solver->x);

// free memory
	gsl_vector_free(stepsize);
	gsl_multimin_fminimizer_free(solver);

	return iter;
}
