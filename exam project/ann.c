#include"header.h"
// ann: artificial neural network
// nw: network

ann* ann_alloc(int n, double(*g)(double), double(*dg)(double)){ 
	ann* nw = malloc(sizeof(ann)); 
	nw->n = n; // number of hidden neurons
	nw->g = g; // activation function
	nw->dg = dg; // activation function derivative
	nw->params = gsl_vector_alloc(3*n); // three parameters per neuron
	return nw;
}

void ann_free(ann* nw){
	gsl_vector_free(nw->params);
	free(nw);
}

void ann_feed_forward(ann* nw, double xk, gsl_vector* F){
	int n = nw->n;
	double sum_F = 0, sum_dF = 0;
	for(int i = 0; i < n; i++){
		double ai = gsl_vector_get(nw->params, 3*i);
		double bi = gsl_vector_get(nw->params, 3*i + 1);
		double wi = gsl_vector_get(nw->params, 3*i + 2);
		sum_F += (nw->g((xk + ai)/bi)) *wi;
		sum_dF += (nw->dg((xk + ai)/bi)) *wi/bi;
	}
	gsl_vector_set(F,0,sum_F);
	gsl_vector_set(F,1,sum_dF);
}

void ann_train(ann* nw, gsl_vector* x, double(*f)(double, double), double x0, double y0, double acc){
	int N = x->size; // number of data points
	// allocate memory
	gsl_vector* F = gsl_vector_alloc(2); // network response and its derivative
	gsl_vector* F_0 = gsl_vector_alloc(2);
	// deviation function (nested)
	double deviation(const gsl_vector* p, void* nothing){
		gsl_vector_memcpy(nw->params, p);
		double sum = 0;
		for(int k = 0; k < N; k++){
			double xk = gsl_vector_get(x,k);
			ann_feed_forward(nw, xk, F);
			double Fk = gsl_vector_get(F,0);
			double dFk = gsl_vector_get(F,1);
			double fk = f(xk, Fk);
			sum += pow(dFk - fk, 2);
		}
		ann_feed_forward(nw, x0, F_0);
		double F0 = gsl_vector_get(F_0,0);
		return sum + N*pow(F0 - y0, 2);
	}
	// minimization of the deviation
	int iter = minimization(deviation, nw->params, acc); // params change
	double dev = deviation(nw->params,NULL);
	fprintf(stderr,"ann_train: iter = %i, deviation = "FMT" \n", iter, dev);
	// free memory
	gsl_vector_free(F);
	gsl_vector_free(F_0);	
}

void ann_solve_ode(int n, gsl_vector* x, double(*f)(double,double), double x0, double y0, double acc){
	// activation function and its derivative
	double g(double x){return x*exp(-x*x/2.0);} 
	double dg(double x){return (1 - x*x)*exp(-x*x/2.0);} 
	// allocate memory
	ann* nw = ann_alloc(n, g, dg);
	gsl_vector* F = gsl_vector_alloc(2);
	// initial parameter values
	double a = gsl_vector_get(x, 0), b = gsl_vector_get(x, x->size - 1);
	for(int i = 0; i < n; i++){
		double ai = a + (b - a) *i/(n - 1);
		double bi = 1;
		double wi = 1;
		gsl_vector_set(nw->params, 3*i, ai); 
		gsl_vector_set(nw->params, 3*i + 1, bi); 
		gsl_vector_set(nw->params, 3*i + 2, wi); 
	}
	// train network
	ann_train(nw, x, f, x0, y0, acc);
	// fit
	printf("x \tF \n");
	double dx = 1e-1;
	for(double xx = a; xx < b; xx += dx){ 
		ann_feed_forward(nw, xx, F);
		double FF = gsl_vector_get(F,0);
		printf(""FMT" \t"FMT" \n", xx, FF);
	}
	printf("\n\n");
	// free memory
	ann_free(nw);
	gsl_vector_free(F);
}
